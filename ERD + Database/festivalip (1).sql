-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 27. Jan 2021 um 13:10
-- Server-Version: 10.4.17-MariaDB
-- PHP-Version: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `festivalip`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `beinhaltet`
--

CREATE TABLE `beinhaltet` (
  `warenkorbId` int(11) NOT NULL,
  `ticketId` int(11) NOT NULL,
  `menge` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `beinhaltet`
--

INSERT INTO `beinhaltet` (`warenkorbId`, `ticketId`, `menge`) VALUES
(1, 1, 2),
(1, 2, 5),
(1, 3, 1),
(5, 1, -6),
(4, 1, 4);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `benutzer`
--

CREATE TABLE `benutzer` (
  `benutzerId` int(11) NOT NULL,
  `vorname` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `nachname` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `nutzername` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `passwort` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `geburtsdatum` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `strasse` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `plz` int(11) NOT NULL,
  `wohnort` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `profilbild` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `benutzer`
--

INSERT INTO `benutzer` (`benutzerId`, `vorname`, `nachname`, `nutzername`, `passwort`, `email`, `geburtsdatum`, `strasse`, `plz`, `wohnort`, `profilbild`) VALUES
(1, 'Anja', 'Brüstle', 'abruestle', 'abruestle', 'a.bruestle@mail.com', '1990-01-19', 'Anjastraße 3', 35429, 'Gießen', 'Pictures/Anja.JPG'),
(2, 'Julia ', 'Rebstock', 'jrebstock', 'jrebstock', 'j.rebstock', '1999-08-05', 'Juliastraße', 35390, 'Gießen', 'Pictures/Julia.JPG'),
(3, 'Jan Ole', 'Schmidt', 'oschmidt', 'oschmidt', 'o.schmidt@mail.com', '1995-05-17', 'olestraße 1', 35390, 'Gießen', 'Pictures/Ole.JPG'),
(4, 'Sebastian ', 'Waldschmidt', 'swaldschmidt', 'swaldschmidt', 's.waldschmidt@mail.com', '1998-01-23', 'Bastistraße 3', 35394, 'Linden', 'Pictures/Basti.JPG'),
(5, 'Niclas ', 'Zimmer', 'nzimmer', 'nzimmer', 'n.zimmer@mail.com', '0199-06-08', 'Hainaer Weg 23', 35444, 'Biebertal', 'Pictures/Niclas.JPG'),
(6, 'Hans', 'Wurst', 'hwurst', 'hwurst', 'h.wurst@mail.com', '1983-01-01', 'Hansstraße 18', 50896, 'Hamburg', ''),
(7, 'Sabine', 'Schubert', 'sschubert', 'sschubert', 's.schuber@mail.com', '1979-05-06', 'Sabinestraße 9', 20687, 'Berlin', ''),
(8, 'Martin ', 'Meier', 'mmeier', 'mmeier', 'm.meier@mail.com', '1997-02-12', 'Martinstraße 89', 34567, 'München', ''),
(9, 'Max', 'Mustermann', 'mmustermann', 'mmustermann', 'm.mustermann@mail.com', '1989-03-20', 'Maxstraße 1', 36548, 'Leipzig', ''),
(10, 'Lar', 'Kurz', 'lkurz', 'lkurz', 'l.kurz@mail.com', '1993-02-12', 'Larsstraße 76', 23678, 'Stuttgart', ''),
(46, 'nutzer', 'nutzer', 'nutzer', 'nutzer', 'nutzer', '0001-11-11', 'nutzer', 12345, 'nutzer', ''),
(47, 'sasad', 'sad', 'asd', 'sad', 'sad', '0001-11-11', 'sadasd', 12345, 'asdsad', ''),
(48, 'Serkan', 'Is', 'myGirl', 'asdf', 'asdf', '0001-11-11', 'serkanstrasse', 12345, 'GirlStadt', '');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `benutzergruppe`
--

CREATE TABLE `benutzergruppe` (
  `benutzergruppenId` int(11) NOT NULL,
  `beschreibung` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `benutzergruppe`
--

INSERT INTO `benutzergruppe` (`benutzergruppenId`, `beschreibung`) VALUES
(1, 'Festivalbesucher'),
(2, 'Festivalveranstallter'),
(3, 'Admin');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `berechtigt`
--

CREATE TABLE `berechtigt` (
  `ticketId` int(11) NOT NULL,
  `festivalId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `berechtigt`
--

INSERT INTO `berechtigt` (`ticketId`, `festivalId`) VALUES
(1, 1),
(2, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `besitzt`
--

CREATE TABLE `besitzt` (
  `warenkorbId` int(11) NOT NULL,
  `benutzerId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `besitzt`
--

INSERT INTO `besitzt` (`warenkorbId`, `benutzerId`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(46, 46),
(47, 47),
(48, 48);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bestellprozess`
--

CREATE TABLE `bestellprozess` (
  `bestellprozessId` int(11) NOT NULL,
  `zahlungsart` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `summe` int(11) NOT NULL,
  `ticketId` int(11) NOT NULL,
  `warenkorbId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `bestellprozess`
--

INSERT INTO `bestellprozess` (`bestellprozessId`, `zahlungsart`, `summe`, `ticketId`, `warenkorbId`) VALUES
(1, 'Paypal', 10, 1, 1),
(2, 'Lastschrift', 20, 2, 2),
(4, 'PayPal', 1, 1, 4);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bewertung`
--

CREATE TABLE `bewertung` (
  `benutzerId` int(11) NOT NULL,
  `festivalId` int(11) NOT NULL,
  `sternebewertung` int(5) NOT NULL,
  `kommentar` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `bewertung`
--

INSERT INTO `bewertung` (`benutzerId`, `festivalId`, `sternebewertung`, `kommentar`) VALUES
(6, 2, 4, 'Wow, echt cool!'),
(7, 1, 5, 'Super!');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `chat`
--

CREATE TABLE `chat` (
  `chatId` int(11) NOT NULL,
  `benutzerId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `chat`
--

INSERT INTO `chat` (`chatId`, `benutzerId`) VALUES
(2, 6),
(1, 7),
(3, 7);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `festival`
--

CREATE TABLE `festival` (
  `festivalId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `stages` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `beschreibung` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `festivalkartenbild` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `genre` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `datum` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `bild` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `href` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `mindestpreis` int(11) NOT NULL,
  `startdatum` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `festival`
--

INSERT INTO `festival` (`festivalId`, `name`, `stages`, `beschreibung`, `festivalkartenbild`, `genre`, `datum`, `bild`, `href`, `mindestpreis`, `startdatum`) VALUES
(1, 'Rock am Ring', 'Feuer, Wasser, Erde, Luft', 'Rock am Ring Festival von 10.02.2021-12.02.2021', '', 'Rock', '14.05. - 16.05.2021\r\n\r\n', 'Pictures/rockamring.png', 'festivals/rockamring.html', 34, '2021-06-11'),
(2, 'Hurricane', 'Feuer, Wasser, Erde, Luft', 'Hurricane Festival von 15.02.2021-17.02.2021', '', 'Pop', '20.08. - 25.08.2021\r\n\r\n', 'Pictures/hurricane.png', 'festivals/hurricane.html', 20, '2021-06-18'),
(3, 'Nature One - Festival', 'Feuer, Wasser, Erde, Luft', 'Nature One - Festival von 30.07.2020 - 01.08.2020', '', 'Techno', '30.07. - 01.08.2021\r\n\r\n', 'Pictures/nature%20one.png', 'festivals/saufside.html', 18, '2020-07-30');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `gruppenzuteilung`
--

CREATE TABLE `gruppenzuteilung` (
  `benutzergruppenId` int(11) NOT NULL,
  `benutzerId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `gruppenzuteilung`
--

INSERT INTO `gruppenzuteilung` (`benutzergruppenId`, `benutzerId`) VALUES
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(1, 6),
(1, 7),
(1, 8),
(2, 9),
(2, 10),
(1, 46),
(1, 47),
(1, 48);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `hat`
--

CREATE TABLE `hat` (
  `stageId` int(11) NOT NULL,
  `festivalId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `hat`
--

INSERT INTO `hat` (`stageId`, `festivalId`) VALUES
(3, 2),
(4, 2),
(1, 2),
(2, 2),
(3, 1),
(4, 1),
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lineup`
--

CREATE TABLE `lineup` (
  `lineupId` int(11) NOT NULL,
  `kuenstler` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `datum` date NOT NULL,
  `stageId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `lineup`
--

INSERT INTO `lineup` (`lineupId`, `kuenstler`, `datum`, `stageId`) VALUES
(1, 'abc', '2021-01-29', 3),
(2, 'xyz', '2021-01-29', 4),
(3, 'asd', '2021-01-29', 1),
(4, 'fgh', '2021-01-29', 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `message`
--

CREATE TABLE `message` (
  `messageId` int(11) NOT NULL,
  `inhalt` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `chatId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `message`
--

INSERT INTO `message` (`messageId`, `inhalt`, `chatId`) VALUES
(1, 'COOL!!', 1),
(2, 'WOW', 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `stage`
--

CREATE TABLE `stage` (
  `stageId` int(11) NOT NULL,
  `beschreibung` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `buttonpositionX` int(11) NOT NULL,
  `buttonpositionY` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `stage`
--

INSERT INTO `stage` (`stageId`, `beschreibung`, `buttonpositionX`, `buttonpositionY`) VALUES
(1, 'Firestage', 0, 0),
(2, 'Waterstage', 0, 0),
(3, 'Airstage', 0, 0),
(4, 'Dirtstage', 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ticket`
--

CREATE TABLE `ticket` (
  `ticketId` int(11) NOT NULL,
  `ticketBild` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL,
  `preis` int(11) NOT NULL,
  `beschreibung` varchar(255) COLLATE utf8mb4_german2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `ticket`
--

INSERT INTO `ticket` (`ticketId`, `ticketBild`, `preis`, `beschreibung`) VALUES
(1, '', 80, 'Festivalticket Standard'),
(2, '', 120, 'Festivalticket VIP'),
(3, '', 30, 'Festivaltagesticket');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `veranstaltet`
--

CREATE TABLE `veranstaltet` (
  `benutzerId` int(11) NOT NULL,
  `festivalId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `veranstaltet`
--

INSERT INTO `veranstaltet` (`benutzerId`, `festivalId`) VALUES
(10, 2),
(9, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `verfuegt`
--

CREATE TABLE `verfuegt` (
  `stageId` int(11) NOT NULL,
  `chatId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `verfuegt`
--

INSERT INTO `verfuegt` (`stageId`, `chatId`) VALUES
(3, 1),
(4, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `warenkorb`
--

CREATE TABLE `warenkorb` (
  `warenkorbId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci;

--
-- Daten für Tabelle `warenkorb`
--

INSERT INTO `warenkorb` (`warenkorbId`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(46),
(47),
(48),
(49);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `beinhaltet`
--
ALTER TABLE `beinhaltet`
  ADD KEY `ticketId` (`ticketId`),
  ADD KEY `warenkorbId` (`warenkorbId`);

--
-- Indizes für die Tabelle `benutzer`
--
ALTER TABLE `benutzer`
  ADD PRIMARY KEY (`benutzerId`);

--
-- Indizes für die Tabelle `benutzergruppe`
--
ALTER TABLE `benutzergruppe`
  ADD PRIMARY KEY (`benutzergruppenId`);

--
-- Indizes für die Tabelle `berechtigt`
--
ALTER TABLE `berechtigt`
  ADD KEY `festivalId` (`festivalId`),
  ADD KEY `ticketId` (`ticketId`);

--
-- Indizes für die Tabelle `besitzt`
--
ALTER TABLE `besitzt`
  ADD KEY `benutzerId` (`benutzerId`),
  ADD KEY `warenkorbId` (`warenkorbId`);

--
-- Indizes für die Tabelle `bestellprozess`
--
ALTER TABLE `bestellprozess`
  ADD PRIMARY KEY (`bestellprozessId`),
  ADD KEY `ticketId` (`ticketId`),
  ADD KEY `warenkorbId` (`warenkorbId`);

--
-- Indizes für die Tabelle `bewertung`
--
ALTER TABLE `bewertung`
  ADD KEY `benutzerId` (`benutzerId`),
  ADD KEY `festivalId` (`festivalId`);

--
-- Indizes für die Tabelle `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`chatId`),
  ADD KEY `benutzerId` (`benutzerId`);

--
-- Indizes für die Tabelle `festival`
--
ALTER TABLE `festival`
  ADD PRIMARY KEY (`festivalId`);

--
-- Indizes für die Tabelle `gruppenzuteilung`
--
ALTER TABLE `gruppenzuteilung`
  ADD KEY `benutzergruppenId` (`benutzergruppenId`),
  ADD KEY `benutzerId` (`benutzerId`);

--
-- Indizes für die Tabelle `hat`
--
ALTER TABLE `hat`
  ADD KEY `festivalId` (`festivalId`),
  ADD KEY `stageId` (`stageId`);

--
-- Indizes für die Tabelle `lineup`
--
ALTER TABLE `lineup`
  ADD PRIMARY KEY (`lineupId`),
  ADD KEY `stageId` (`stageId`);

--
-- Indizes für die Tabelle `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`messageId`),
  ADD KEY `chatId` (`chatId`);

--
-- Indizes für die Tabelle `stage`
--
ALTER TABLE `stage`
  ADD PRIMARY KEY (`stageId`);

--
-- Indizes für die Tabelle `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`ticketId`);

--
-- Indizes für die Tabelle `veranstaltet`
--
ALTER TABLE `veranstaltet`
  ADD KEY `benutzerId` (`benutzerId`),
  ADD KEY `festivalId` (`festivalId`);

--
-- Indizes für die Tabelle `verfuegt`
--
ALTER TABLE `verfuegt`
  ADD KEY `chatId` (`chatId`),
  ADD KEY `stageId` (`stageId`);

--
-- Indizes für die Tabelle `warenkorb`
--
ALTER TABLE `warenkorb`
  ADD PRIMARY KEY (`warenkorbId`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `benutzer`
--
ALTER TABLE `benutzer`
  MODIFY `benutzerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT für Tabelle `bestellprozess`
--
ALTER TABLE `bestellprozess`
  MODIFY `bestellprozessId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `chat`
--
ALTER TABLE `chat`
  MODIFY `chatId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `festival`
--
ALTER TABLE `festival`
  MODIFY `festivalId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `lineup`
--
ALTER TABLE `lineup`
  MODIFY `lineupId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `message`
--
ALTER TABLE `message`
  MODIFY `messageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT für Tabelle `stage`
--
ALTER TABLE `stage`
  MODIFY `stageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `ticket`
--
ALTER TABLE `ticket`
  MODIFY `ticketId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `warenkorb`
--
ALTER TABLE `warenkorb`
  MODIFY `warenkorbId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `beinhaltet`
--
ALTER TABLE `beinhaltet`
  ADD CONSTRAINT `beinhaltet_ibfk_1` FOREIGN KEY (`ticketId`) REFERENCES `ticket` (`ticketId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `beinhaltet_ibfk_2` FOREIGN KEY (`warenkorbId`) REFERENCES `warenkorb` (`warenkorbId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `berechtigt`
--
ALTER TABLE `berechtigt`
  ADD CONSTRAINT `berechtigt_ibfk_1` FOREIGN KEY (`festivalId`) REFERENCES `festival` (`festivalId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `berechtigt_ibfk_2` FOREIGN KEY (`ticketId`) REFERENCES `ticket` (`ticketId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `besitzt`
--
ALTER TABLE `besitzt`
  ADD CONSTRAINT `besitzt_ibfk_1` FOREIGN KEY (`benutzerId`) REFERENCES `benutzer` (`benutzerId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `besitzt_ibfk_2` FOREIGN KEY (`warenkorbId`) REFERENCES `warenkorb` (`warenkorbId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `bestellprozess`
--
ALTER TABLE `bestellprozess`
  ADD CONSTRAINT `bestellprozess_ibfk_1` FOREIGN KEY (`ticketId`) REFERENCES `ticket` (`ticketId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bestellprozess_ibfk_2` FOREIGN KEY (`warenkorbId`) REFERENCES `warenkorb` (`warenkorbId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `bewertung`
--
ALTER TABLE `bewertung`
  ADD CONSTRAINT `bewertung_ibfk_1` FOREIGN KEY (`benutzerId`) REFERENCES `benutzer` (`benutzerId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bewertung_ibfk_2` FOREIGN KEY (`festivalId`) REFERENCES `festival` (`festivalId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `chat_ibfk_1` FOREIGN KEY (`benutzerId`) REFERENCES `benutzer` (`benutzerId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `gruppenzuteilung`
--
ALTER TABLE `gruppenzuteilung`
  ADD CONSTRAINT `gruppenzuteilung_ibfk_1` FOREIGN KEY (`benutzergruppenId`) REFERENCES `benutzergruppe` (`benutzergruppenId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `gruppenzuteilung_ibfk_2` FOREIGN KEY (`benutzerId`) REFERENCES `benutzer` (`benutzerId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `hat`
--
ALTER TABLE `hat`
  ADD CONSTRAINT `hat_ibfk_1` FOREIGN KEY (`festivalId`) REFERENCES `festival` (`festivalId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hat_ibfk_2` FOREIGN KEY (`stageId`) REFERENCES `stage` (`stageId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `lineup`
--
ALTER TABLE `lineup`
  ADD CONSTRAINT `lineup_ibfk_1` FOREIGN KEY (`stageId`) REFERENCES `stage` (`stageId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_ibfk_1` FOREIGN KEY (`chatId`) REFERENCES `chat` (`chatId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `veranstaltet`
--
ALTER TABLE `veranstaltet`
  ADD CONSTRAINT `veranstaltet_ibfk_1` FOREIGN KEY (`benutzerId`) REFERENCES `benutzer` (`benutzerId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `veranstaltet_ibfk_2` FOREIGN KEY (`festivalId`) REFERENCES `festival` (`festivalId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints der Tabelle `verfuegt`
--
ALTER TABLE `verfuegt`
  ADD CONSTRAINT `verfuegt_ibfk_1` FOREIGN KEY (`chatId`) REFERENCES `chat` (`chatId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `verfuegt_ibfk_2` FOREIGN KEY (`stageId`) REFERENCES `stage` (`stageId`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
