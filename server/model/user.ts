// Class representing a group
export class Group {
    public benutzergruppenId:number;
    public beschreibung: string;

    constructor(benutzergruppenId:number, beschreibung:string){
        this.benutzergruppenId = benutzergruppenId;
        this.beschreibung = beschreibung;
    }
}

// Class representing a user
export class User {
    public benutzerId: number;
    public vorname: string;
    public nachname: string;
    public nutzername: string;
    public profilbild: string;
    public passwort: string;
    public email: string;
    public geburtsdatum: string;
    public strasse: string;
    public plz: number;
    public wohnort: string;
    public groups: Group[] = [];


    constructor(benutzerId:number, vorname: string, nachname: string,
                nutzername: string,  profilbild: string, passwort: string,
                email: string, geburtsdatum: string, strasse: string, plz: number, wohnort: string) {

        this.benutzerId = benutzerId;
        this.vorname = vorname;
        this.nachname = nachname;
        this.nutzername = nutzername;
        this.profilbild = profilbild;
        this.passwort = passwort;
        this.email = email;
        this.geburtsdatum = geburtsdatum;
        this.strasse = strasse;
        this.plz = plz;
        this.wohnort = wohnort;
    }
}

// Class representing a ticket
export class Ticket{
    public ticketId: number;
    public preis: number;
    public beschreibung: string;

    constructor(ticketId: number, preis: number, beschreibung: string) {
        this.ticketId = ticketId;
        this.preis = preis;
        this.beschreibung = beschreibung;
    }
}

// Class representing a ticket
export class Festival{
    public festivalId: number;
    public name: string;
    public stages: string;
    public beschreibung: string;
    public festivalkartenbild: string;
    public genre: string;
    public datum: string;
    public bild: string;
    public href: string;
    public startdatum: string;
    public mindestpreis: number;


    constructor(festivalId: number, name: string, stages: string, beschreibung: string, festivalkartenbild: string, genre: string, datum: string, bild: string, href: string, startdatum: string, mindestpreis: number) {
    this.festivalId=festivalId;
    this.name=name;
    this.stages=stages;
    this.beschreibung = beschreibung;
    this.festivalkartenbild = festivalkartenbild;
    this.genre = genre;
    this.datum = datum;
    this.bild = bild;
    this.href = href;
    this.startdatum = startdatum;
    this.mindestpreis = mindestpreis;

    }
}


// Class representing a warenkorb
export class Warenkorb{
    public beschreibung: string;
    public preis: number;
    public ticketId: number
    public menge: number

    constructor(beschreibung: string, preis: number, ticketId: number, menge:number) {
        this.beschreibung=beschreibung;
        this.preis=preis;
        this.ticketId=ticketId;
        this.menge=menge;
    }
}
