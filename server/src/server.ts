/*****************************************************************************
 * Import package                                                            *
 *****************************************************************************/
//Imports all required packages
import express = require ('express'); //needed for routing, express library gets involved
import mysql = require('mysql');
import session = require('express-session');
import {Request, Response} from 'express'; //Request/Response from Express
import {User, Group, Ticket, Festival, Warenkorb} from "../model/user";
import {Connection, MysqlError} from 'mysql'; //import of database module


/*****************************************************************************
 * Define and start web-app server, define json-Parser                       *
 *****************************************************************************/
//Use express, create server with handler function and start it under port 8080 and console.log it
const app = express();
app.listen(8080, () => {
    console.log('Server started: http://localhost:8080');
});

//Configuration for Database connection
const database: Connection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: '',
    database: 'festivalip'
});

// Connect the database and console.log it if the connection failed or if it is connected
database.connect((err: MysqlError) => {
    if (err) {
        console.log('Database connection failed: ', err);
    } else {
        console.log('Database is connected');
    }
});

// use session to create a cookie for the login to show different roles different content
app.use(session({
    resave: true,
    saveUninitialized: true,
    rolling: true,
    secret: "geheim",
    name: "mySessionCookie",
    cookie: {maxAge: 5 * 60 * 1000}
}));

//define json parser
app.use(express.json());
declare module 'express-session' {
    interface SessionData {
        username: string,
        role: string,
        userId: number
    }
}
/*****************************************************************************
 * STATIC ROUTES                                                             *
 *****************************************************************************/
//construction of base directory (if server gets started and URL starts with ... data in directory ... gets transfered)
const basedir: string = __dirname + '/../..';  // get rid of /server/src
app.use('/', express.static(basedir + '/client/views'));
app.use('/css', express.static(basedir + '/client/css'));
app.use('/src', express.static(basedir + '/client/src'));
app.use('/jquery', express.static(basedir + '/client/node_modules/jquery/dist'));
app.use('/popperjs', express.static(basedir + '/node_modules/popper.js/dist'));
app.use('/bootstrap', express.static(basedir + '/node_modules/bootstrap/dist'));
app.use('/font-awesome', express.static(basedir + '/node_modules/font-awesome/css'));


/*****************************************************************************
 * HTTP ROUTES: USER, USERS                                                  *
 *****************************************************************************/

//deletes the Profile
/**
 * @api {delete} /deleteProfile Deletes the profile from the user
 * @apiName deleteProfile
 * @apiGroup Profile
 * @apiVersion 2.0.0
 *
 * @apiParam {number} userId The id of the requested user
 *
 * @apiSuccess {string} message Message stating the user has been deleted
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "message":"Successfully deleted profile"
 * }
 *
 * @apiError (Client Error) {500} NotAllMandatoryFields The request did not contain all mandatory fields
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"Database request failed" + err
 * }
 */
app.delete('/deleteProfile', (req: Request, res: Response)=> {
    const data: number = req.session.userId;

    let query: string = "DELETE FROM `benutzer` WHERE `benutzerId` =?"
    database.query(query, data, (err: MysqlError, rows: any) => {
        if (err) {
            //error response
            res.status(500).send({
                message: "database request failed:" + err
            })
        } else {
            res.status(200).send({
                message: "Successfully deleted Profile"
            })
        }
    });
})

//Post the ticketId inside the warenkorb
/**
 * @api {post} /addToCart/:ticketId Post the ticketId inside the warenkorb
 * @apiName postAddToCart
 * @apiGroup addToCart
 * @apiVersion 2.0.0
 *
 * @apiParam {number} ticketId Read the ticketId
 * @apiParam {number} userid Userid is the same id as the warenkorbId
 * @apiParam {array} shows the userId and the ticketId
 * @apiParam {string} query Fill warenkorbId and ticketId inside beinhaltet with the values ?
 *
 * @apiSuccess {string} message Message stating the users have been found
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "message":"Successfully added to cart"
 * }
 *
 * @apiError (Client Error) {500} NotAllMandatoryFields The request did not contain all mandatory fields
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"database request failed" + err
 * }
 */
app.post('/addToCart/:ticketId', (req: Request, res: Response)=>{
    const ticketId: number = Number(req.params.ticketId);
    let userId: number= req.session.userId; //User Id is the same id as the warenkorbId.
    let data:[number, number]=[userId, ticketId];
    let query: string="INSERT INTO `beinhaltet` (`warenkorbId`, `ticketId`, `menge`) VALUES (?, ?, '1');"
    database.query(query, data, (err: MysqlError, rows: any) => {
        if (err) {
            //error response
            res.status(500).send({
                message: "database request failed:" + err
            })
        }else {
            //success response
            res.status(200).send({
                message: 'Successfully added to cart'
            });

        }
    });
});

// This function is looking for a saved cookie (needed to check if user is logged in), checks if session is fitting to ip of user
function isLoggedIn() {
    return (req: Request, res: Response, next) => {
        //read data from request body, with req.session session gets checked via cookie
        if (req.session.username) {
            //next skips else part and runs route where function isLoggedIn is used
            next();
        } else {
            //send response
            res.status(401).send({
                message: "Bitte einloggen"
            })
        }
    }
}

// This function is looking if the userId which is saved in the session, equals to the warenkorbId. Remember, the warenkorbId is everytime the same as the userId.
/**
 * @api {get} /checkWatchStream Check if the User can watch the stream
 * @apiName getCheckWatchStream
 * @apiGroup checkWatchStream
 * @apiVersion 2.0.0
 *
 * @apiParam {number} data Read the session userId
 *
 * @apiSuccess {string} message Message the user can watch the stream
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "message":"You can watch the Stream"
 * }
 *
 *  @apiError (Client Error) {500} DatabaseRequestFailed The database is not connected
 * @apiError (Client Error) {401} NoPermissions Not enough permissions
 *
 * @apiErrorExample DatabaseRequestFailed:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"There was an error" +err
 * }
 *
 * @apiErrorExample NoPermissions:
 * HTTP/1.1 401 Bad Request
 * {
 *     "message":"You don't have enough permissions"
 * }
 */
app.get('/checkWatchStream', (req: Request, res: Response)=>{
    let data=req.session.userId;

    let query:string="SELECT * FROM `bestellprozess` WHERE `warenkorbId` = ?";

    database.query(query, data, (err: MysqlError, rows: any) => {
        if (err) {
            //error response
            res.status(500).send({
                message: "database request failed:" + err
            })
        }if (rows.length ===1){
            res.status(200).send({
            message: "You can watch the Stream"
        })
        }else{
        res.status(401).send({
            message:"You don't have enough permissions"
        });
        }
    })
})


// This route is checking if someone is still logged in
/**
 * @api {get} /login Check if the User is still logged in
 * @apiName getLogin
 * @apiGroup login
 * @apiVersion 2.0.0
 *
 * @apiSuccess {string} message Message stating the users have been found
 * @apiSuccess {string} user It shows the Username
 * @apiSuccess {string} profile It shows the Username
 * @apiSuccess {string} role It shows the role of the user
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "message":"User" + username + " still logged in"
 * }
 */
app.get('/login', isLoggedIn(), (req: Request, res: Response) => {
    res.status(200).send({
        //response, session control
        message: "User " + req.session.username + " still logged in",
        user: req.session.username,
        profile: req.session.username,
        role: req.session.role
    })
});

//Get the warenkorb
/**
 * @api {get} /warenkorb Get the warenkorb
 * @apiName getWarenkorb
 * @apiGroup warenkorb
 * @apiVersion 2.0.0
 *
 * @apiParam {string} data Read the username
 * @apiParam {string} query Get the warenkorb from each user
 *
 * @apiSuccess {warenkorb[]} warenkorb The list of the warenkorb
 * @apiSuccess {string} message Message stating the users have been found
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "warenkorb": [
 *      {
 *        "beschreibung": "warenkorb1",
 *        "preis": "100€",
 *        "ticketid": "1",
 *        "menge": "1"
 *     },
 *    ]
 *    "message":"Successfully requested shopping list"
 * }
 *
 * @apiError (Client Error) {500} NotAllMandatoryFields The request did not contain all mandatory fields
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"database request failed" + err
 * }
 */
app.get('/warenkorb', (req: Request, res: Response) => {
    //routes the request //sql string to get the warenkorb, define database query
    let data: number= req.session.userId;
    let query: string = "SELECT `benutzer`.`nutzername`, `besitzt`.*, `warenkorb`.*, `beinhaltet`.*, `ticket`.* FROM `benutzer` LEFT JOIN `besitzt` ON `besitzt`.`benutzerId` = `benutzer`.`benutzerId` LEFT JOIN `warenkorb` ON `besitzt`.`warenkorbId` = `warenkorb`.`warenkorbId` LEFT JOIN `beinhaltet` ON `beinhaltet`.`warenkorbId` = `warenkorb`.`warenkorbId` LEFT JOIN `ticket` ON `beinhaltet`.`ticketId` = `ticket`.`ticketId` WHERE `benutzer`.`benutzerId` = ?"

    //to run the database query
    database.query(query, data, (err: MysqlError, rows: any) => {
        if (err) {
            //error response
            res.status(500).send({
                message: "database request failed:" + err
            })
        } else {
            //userList as an empty array, users get pushed into array with for loop
            let shoppingList: Warenkorb[] = [];
            let list: Warenkorb;
            for (const row of rows) {
                list = new Warenkorb(
                    row.beschreibung,
                    row.preis,
                    row.ticketId,
                    row.menge
                );
                shoppingList.push(list);
            }
            //success response
            res.status(200).send({
                shoppingList: shoppingList,
                message: 'Successfully requested shopping list'
            });
        }
    })
})

//Get a list of all ticktes
/**
 * @api {get} /alleTickets Get all tickets
 * @apiName getAlleTickets
 * @apiGroup alleTickets
 * @apiVersion 2.0.0
 *
 * @apiParam {string} query All tickets ordered by the ticketId
 *
 * @apiSuccess {Ticket[]} ticketList The list of all tickets ordered by festivalId
 * @apiSuccess {string} message Message stating the users have been found
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "ticketList": [
 *      {
 *        "ticketId": "1",
 *        "preis": "40€",
 *        "beschreibung": "Standardticket",
 *     },
 *    ]
 *    "message":"Successfully requested ticket list"
 * }
 *
 * @apiError (Client Error) {500} NotAllMandatoryFields The request did not contain all mandatory fields
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"database request failed" + err
 * }
 */
app.get('/alleTickets', (req: Request, res: Response) => {
    //routes the request //sql string to get all users ordered by id descending, define database query
    let query: string = 'SELECT * FROM `ticket` ORDER BY `ticketId` DESC';

    //to run the database query
    database.query(query, (err: MysqlError, rows: any) => {
        if (err) {
            //error response
            res.status(500).send({
                message: "database request failed:" + err
            })
        } else {
            //userList as an empty array, users get pushed into array with for loop
            let ticketlist: Ticket[] = [];
            let ticket: Ticket;
            for (const row of rows) {
                ticket = new Ticket(
                    row.ticketId,
                    row.preis,
                    row.beschreibung,
                );
                ticketlist.push(ticket);
            }
            //success response
            res.status(200).send({
                ticketList: ticketlist,
                message: 'Successfully requested ticket list'
            });
        }
    })
})

//Get a list of all festivals
/**
 * @api {get} /alleFestivals Get all festivals
 * @apiName getAlleFestivals
 * @apiGroup AlleFestivals
 * @apiVersion 2.0.0
 *
 * @apiParam {string} query All festivals ordered by the festivalId
 *
 * @apiSuccess {Festival[]} festivalList The list of all festivals ordered by festivalId
 * @apiSuccess {string} message Message stating the users have been found
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "festivalList": [
 *      {
 *        "festivalId": "3",
 *        "name": "Rock am Ring",
 *        "genre": "rock",
 *        "beschreibung": "Dies ist ein Rock Festival",
 *        "festivalkartenbild": "",
 *        "genre": "rock",
 *        "datum": "14.05.-16.05.2021",
 *        "bild": "",
 *        "href": "",
 *     },
 *    ]
 *    "message":"Successfully requested festival list"
 * }
 *
 * @apiError (Client Error) {500} NotAllMandatoryFields The request did not contain all mandatory fields
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"database request failed" + err
 * }
 */
app.get('/alleFestivals', (req: Request, res: Response) => {
    //routes the request //sql string to get all users ordered by id descending, define database query
    let query: string = 'SELECT * FROM `festival` ORDER BY `festivalId` DESC';

    //to run the database query
    database.query(query, (err: MysqlError, rows: any) => {
        if (err) {
            //error response
            res.status(500).send({
                message: "database request failed:" + err
            })
        } else {
            //userList as an empty array, users get pushed into array with for loop
            let festivalList: Festival[] = [];
            let festival: Festival;
            for (const row of rows) {
                festival = new Festival(
                    row.festivalId,
                    row.name,
                    row.genre,
                    row.beschreibung,
                    row.festivalkartenbild,
                    row.genre,
                    row.datum,
                    row.bild,
                    row.href,
                    row.startdatum,
                    row.mindestpreis,
                );
                festivalList.push(festival);
            }
            //success response
            res.status(200).send({
                festivalList: festivalList,
                message: 'Successfully requested festival list'
            });
        }
    })
})

//Get all festivals with the genre Rock
/**
 * @api {get} /rockfestivals Get all festivals with the genre rock
 * @apiName getRockFestivals
 * @apiGroup RockFestivals
 * @apiVersion 2.0.0
 *
 * @apiParam {string} query All festivals with the genre rock
 *
 * @apiSuccess {Festival[]} festivalList The list of the festivals with the genre rock
 * @apiSuccess {string} message Message stating the users have been found
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "festivalList": [
 *      {
 *        "festivalId": "3",
 *        "name": "Rock am Ring",
 *        "genre": "rock",
 *        "beschreibung": "Dies ist ein Rock Festival",
 *        "festivalkartenbild": "",
 *        "genre": "rock",
 *        "datum": "14.05.-16.05.2021",
 *        "bild": "",
 *        "href": "",
 *     },
 *    ]
 *    "message":"Successfully requested festival list"
 * }
 *
 * @apiError (Client Error) {500} NotAllMandatoryFields The request did not contain all mandatory fields
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"database request failed" + err
 * }
 */
app.get('/rockfestivals', (req: Request, res: Response) => {
    //routes the request //sql string to get all users ordered by id descending, define database query
    let query: string = "SELECT * FROM `festival` WHERE `genre` = 'Rock'";

    //to run the database query
    database.query(query, (err: MysqlError, rows: any) => {
        if (err) {
            //error response
            res.status(500).send({
                message: "database request failed:" + err
            })
        } else {
            //userList as an empty array, users get pushed into array with for loop
            let festivalList: Festival[] = [];
            let festival: Festival;
            for (const row of rows) {
                festival = new Festival(
                    row.festivalId,
                    row.name,
                    row.genre,
                    row.beschreibung,
                    row.festivalkartenbild,
                    row.genre,
                    row.datum,
                    row.bild,
                    row.href,
                    row.startdatum,
                    row.mindestpreis,
                );
                festivalList.push(festival);
            }
            //success response
            res.status(200).send({
                festivalList: festivalList,
                message: 'Successfully requested festival list'
            });
        }
    })
})

//Get all festivals with the genre Pop
/**
 * @api {get} /popfestivals Get all festivals with the genre Pop
 * @apiName getPopFestivals
 * @apiGroup popFestivals
 * @apiVersion 2.0.0
 *
 * @apiParam {string} query All festivals with the genre pop
 *
 * @apiSuccess {Festival[]} festivalList The list of the festivals with the genre pop
 * @apiSuccess {string} message Message stating the users have been found
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "festivalList": [
 *      {
 *        "festivalId": "2",
 *        "name": "sauside",
 *        "genre": "pop",
 *        "beschreibung": "Dies ist ein Pop Festival",
 *        "festivalkartenbild": "",
 *        "genre": "pop",
 *        "datum": "01.08.-05.08.2021",
 *        "bild": "",
 *        "href": "",
 *     },
 *    ]
 *    "message":"Successfully requested festival list"
 * }
 *
 * @apiError (Client Error) {500} NotAllMandatoryFields The request did not contain all mandatory fields
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"database request failed" + err
 * }
 */
app.get('/popfestivals', (req: Request, res: Response) => {
    //routes the request //sql string to get all users ordered by id descending, define database query
    //let query: string = "SELECT * FROM `festival` WHERE `genre` = '?'";
    let query: string = "SELECT * FROM `festival` WHERE `genre` = 'Pop'";
    //to run the database query
    database.query(query, (err: MysqlError, rows: any) => {
        if (err) {
            //error response
            res.status(500).send({
                message: "database request failed:" + err
            })
        } else {
            //userList as an empty array, users get pushed into array with for loop
            let festivalList: Festival[] = [];
            let festival: Festival;
            for (const row of rows) {
                festival = new Festival(
                    row.festivalId,
                    row.name,
                    row.genre,
                    row.beschreibung,
                    row.festivalkartenbild,
                    row.genre,
                    row.datum,
                    row.bild,
                    row.href,
                    row.startdatum,
                    row.mindestpreis,
                );
                festivalList.push(festival);
            }
            //success response
            res.status(200).send({
                festivalList: festivalList,
                message: 'Successfully requested festival list'
            });
        }
    })
})

//Get all festivals with the genre Techno
/**
 * @api {get} /technofestivals Get all festivals with the genre Techno
 * @apiName getTechnoFestivals
 * @apiGroup technoFestivals
 * @apiVersion 2.0.0
 *
 * @apiParam {string} query All festivals with the genre techno
 *
 * @apiSuccess {Festival[]} festivalList The list of the festivals with the genre techno
 * @apiSuccess {string} message Message stating the users have been found
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "festivalList": [
 *      {
 *        "festivalId": "1",
 *        "name": "Hurricane",
 *        "genre": "techno",
 *        "beschreibung": "Dies ist ein Techno Festival",
 *        "festivalkartenbild": "",
 *        "genre": "techno",
 *        "datum": "20.08.-25.08.2021",
 *        "bild": "",
 *        "href": "",
 *     },
 *    ]
 *    "message":"Successfully requested festival list"
 * }
 *
 * @apiError (Client Error) {500} NotAllMandatoryFields The request did not contain all mandatory fields
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"database request failed" + err
 * }
 */
app.get('/technofestivals', (req: Request, res: Response) => {
    //routes the request //sql string to get all festivals with the genre techno, define database query
    //let query: string = "SELECT * FROM `festival` WHERE `genre` = '?'";
    let query: string = "SELECT * FROM `festival` WHERE `genre` = 'Techno'";
    //to run the database query
    database.query(query, (err: MysqlError, rows: any) => {
        if (err) {
            //error response
            res.status(500).send({
                message: "database request failed:" + err
            })
        } else {
            //festivalList as an empty array, users get pushed into array with for loop
            let festivalList: Festival[] = [];
            let festival: Festival;
            for (const row of rows) {
                festival = new Festival(
                    row.festivalId,
                    row.name,
                    row.genre,
                    row.beschreibung,
                    row.festivalkartenbild,
                    row.genre,
                    row.datum,
                    row.bild,
                    row.href,
                    row.startdatum,
                    row.mindestpreis,
                );
                festivalList.push(festival);
            }
            //success response
            res.status(200).send({
                festivalList: festivalList,
                message: 'Successfully requested festival list'
            });
        }
    })
})

//Get the requested User to the Userprofil
/**
 * @api {get} /userprofil Get one benutzer
 * @apiName getUserProfil
 * @apiGroup userProfil
 * @apiVersion 2.0.0
 *
 * @apiParam {string} data Read the username
 * @apiParam {string} query Get the User from the value "?"
 *
 * @apiSuccess {User[]} userList The list of one user
 * @apiSuccess {string} message Message stating the users have been found
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "userList": [
 *      {
 *        "benutzerId": "1",
 *        "vorname": "Hans",
 *        "nachname": "Mustermann",
 *        "nutzername": "mmustermmann",
 *        "profilbild": "",
 *        "passwort": "mmustermann",
 *        "email": "m.mustermann@emai.com",
 *        "geburtsdatum": "11.11.2011",
 *        "strasse": "mmustermannstrasse",
 *        "plz": "111111",
 *        "wohnort": "musterort",
 *     },
 *    ]
 *    "message":"Successfully requested user list"
 * }
 *
 * @apiError (Client Error) {400} NotAllMandatoryFields The request did not contain all mandatory fields
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 400 Bad Request
 * {
 *     "message":"database request failed" + innererr
 * }
 */
app.get('/userProfil',isLoggedIn(), (req: Request, res: Response) => {
    let data: string = req.session.username;

    let query: string = "SELECT `benutzer`.*, `benutzer`.`nutzername` FROM `benutzer` WHERE `benutzer`.`nutzername` = ?";

    database.query(query, data, (innerErr: MysqlError, rows: any) => {
        // Database operation has failed
        if (innerErr) {
            //error message
            res.status(400).send({
                message: 'Database request failed.' + innerErr
            })
        } else {
            //userList as an empty array, users get pushed into array with for loop
            let userlist: User[] = [];
            let user: User;
            for (const row of rows) {
                user = new User(
                    row.benutzerId,
                    row.vorname,
                    row.nachname,
                    row.nutzername,
                    row.profilbild,
                    row.passwort,
                    row.email,
                    row.geburtsdatum,
                    row.strasse,
                    row.plz,
                    row.wohnort
                );
                //users get pushed into userlist
                userlist.push(user);
            }
            //success response
            res.status(200).send({
                userList: userlist,
                message: 'Successfully requested user list'
            });
        }
    })
})

// Get All Users
/**
 * @api {get} /benutzer Get all Users
 * @apiName getBenutzer
 * @apiGroup Benutzer
 * @apiVersion 2.0.0
 *
 * @apiParam {string} query Get all Users ordered by Userid
 *
 * @apiSuccess {string} message Message stating the user list has been found
 * @apiSuccess {User[]} userList The list of all users
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * "userList": [
 *      {
 *        "benutzerId": "1",
 *        "vorname": "Hans",
 *        "nachname": "Mustermann",
 *        "nutzername": "mmustermmann",
 *        "profilbild": "",
 *        "passwort": "mmustermann",
 *        "email": "m.mustermann@emai.com",
 *        "geburtsdatum": "11.11.2011",
 *        "strasse": "mmustermannstrasse",
 *        "plz": "111111",
 *        "wohnort": "musterort",
 *     },
 * {
 *     "message":"Successfully requested user list"
 * }
 *
 * @apiError (Client Error) {500} NotAllMandatoryFields The request did not contain all mandatory fields
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"database request failed" + err
 * }
 */
app.get('/benutzer', (req: Request, res: Response) => {
    //routes the request //sql string to get all users ordered by id descending, define database query
    let query: string = 'SELECT * FROM `benutzer` ORDER BY `benutzerId` DESC';

    //to run the database query
    database.query(query, (err: MysqlError, rows: any) => {
        if (err) {
            //error response
            res.status(500).send({
                message: "database request failed:" + err
            })
        } else {
            //userList as an empty array, users get pushed into array with for loop
            let userlist: User[] = [];
            let user: User;
            for (const row of rows) {
                user = new User(
                    row.benutzerId,
                    row.vorname,
                    row.nachname,
                    row.nutzername,
                    row.profilbild,
                    row.passwort,
                    row.email,
                    row.geburtsdatum,
                    row.strasse,
                    row.plz,
                    row.wohnort
                );
                //users get pushed into userlist
                userlist.push(user);
            }
            //success response
            res.status(200).send({
                userList: userlist,
                message: 'Successfully requested user list'
            });
        }
    })
})

//route to get all usergroups
/**
 * @api {get} /groups Get groupslist from all Users
 * @apiName getGroups
 * @apiGroup Groups
 * @apiVersion 2.0.0
 *
 * @apiParam {string} query All datas from benutzergruppe
 *
 * @apiSuccess {string} message Message stating successfully requested the groups list
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "message":"Succesfully requested groups list"
 * }
 *
 * @apiError (Client Error) {500} NotAllMandatoryFields The request did not contain all mandatory fields
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"database request failed" + err
 * }
 */
/*
app.get('/groups', (req: Request, res: Response) => {
    //all usergroups get selected
    const query: string = 'SELECT * FROM `benutzergruppe`';
    database.query(query, (err: MysqlError, rows: any) => {
        if (err) {
            //error response
            res.status(500).send({
                message: "database request failed:" + err
            })
        } else {
            //userList as an empty array, users get pushed into array with for loop
            let grouplist: Group[] = [];
            let group: Group;
            for (const row of rows) {
                group = new Group(
                    row.benutzergruppenId,
                    row.beschreibung,
                );
                grouplist.push(group);
                console.log("test"+grouplist);
            }
            //success response
            res.status(200).send({
                groupList: grouplist,
                message: 'Successfully requested festival list'
            });
        }
    })
})
*/
app.get('/groups', (req: Request, res: Response) => {
    //all usergroups get selected
    const query: string = 'SELECT * FROM `benutzergruppe`';
    database.query(query, (err: MysqlError, rows: any) => {
        if (err) {
            //error response
            res.status(500).send({
                message: "database request failed:" + err
            })
        } else {
            res.status(200).send({
                groups: rows,
                message: "Successfully requested groups list"
            })
        }
    })
})

// This route deletes the session cookie to logout a user
/**
 * @api {post} /logout Logout a user
 * @apiName postLogout
 * @apiGroup Logout
 * @apiVersion 2.0.0
 *
 * @apiSuccess {string} message Message stating the new user has been logged out
 *
 * @apiSuccessExample Success-Response: Logout a User
 * HTTP/1.1 200 OK
 * {
 *     "message":""Sucessfully logged out"
 * }
 */
app.post('/logout', (req: Request, res: Response) => {
    delete req.session.username;
    res.status(200).send({
        message: "Successfully logged out"
    })
});

// This route creates a new cookie if a user with a given username and password exists and its correct. It also check which role the user has.
/**
 * @api {post} /login Login a User
 * @apiName postLogin
 * @apiGroup Login
 * @apiVersion 2.0.0
 *
 * @apiParam {string} username
 * @apiParam {string} password
 *
 * @apiSuccess {string} message Message stating the new user has been logged in
 *
 * @apiSuccessExample Success-Response: Login a User
 * HTTP/1.1 200 OK
 * {
 *     "message":""Logged in as: " + username + " with the Role:" + role"
 * }
 *
 * @apiError (Client Error) {500} NotAllMandatoryFields The request did not contain all mandatory fields
 * @apiError (Client Error) {401} WrongPasswordOrUsername The typed username or password is wrong
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"There was an error"
 * }
 *
 * @apiErrorExample WrongPasswordOrUsername:
 * HTTP/1.1 400 Bad Request
 * {
 *     "message":"Wrong password or username"
 * }
 */
app.post('/login', (req: Request, res: Response) => {
    //read data from request body
    const username: string = req.body.loginname;
    const password: string = req.body.password;

    //constant date gets values of username and body
    const data: [string, string] = [
        username,
        password,
    ];

    //selects username, password (to check profile), usergroup (to assign roles and rights) and checks the compatibility
    const query = ('SELECT `benutzer`.`nutzername`, `benutzer`.`passwort`, `gruppenzuteilung`.*, `benutzergruppe`.`beschreibung` AS "role" FROM `benutzer` LEFT JOIN `gruppenzuteilung` ON `gruppenzuteilung`.`benutzerId` = `benutzer`.`benutzerId` LEFT JOIN `benutzergruppe` ON `gruppenzuteilung`.`benutzergruppenId` = `benutzergruppe`.`benutzergruppenId` WHERE `benutzer`.`nutzername` = ? AND `benutzer`.`passwort` = ?');
    //query and data get transfered
    database.query(query, data, (err: MysqlError, rows: any) => {

        //checks if entered data are in database
        if (err) {
            //error message
            res.status(500).send({
                message: "There was an error"
            });
        } else {
            // if construct gets entered if exactly 1 user is selected
            if (rows.length === 1) {
                //session gets adapted to user and userrights, session.username gets saved as username (to deposit)
                req.session.username = username;
                for (const row of rows) {
                    //role gets checked and saved for user
                    let role = row.role
                    let userId= row.benutzerId
                    req.session.role = role;
                    req.session.userId = userId;

                }
                //success message
                res.status(200).send({
                    message: "Logged in as: " + req.session.username + " with the Role:" + req.session.role,
                    //for display who is logged in
                    username,
                    //to check if role got transfered correctly
                    role: req.session.role
                });
            } else {
                //error message
                res.status(401).send({
                    message: "Wrong password or username"
                })
            }
        }
    })
});

// POST-Route - Create a new user
/**
 * @api {post} /user Create a new user
 * @apiName postUser
 * @apiGroup User
 * @apiVersion 2.0.0
 *
 * @apiParam {string} vorname des Benutzers
 * @apiParam {string} nachname des Benutzers
 * @apiParam {string} nutzername des Benutzers
 * @apiParam {string} passwort des Benutzers
 * @apiParam {string} email des Benutzers
 * @apiParam {string} geburtsdatum des Benutzers
 * @apiParam {string} strasse des Benutzers
 * @apiParam {number} plz des Benutzers
 * @apiParam {string} wohnort des Benutzers
 * @apiParam {string} profilbild des Benutzers
 *
 * @apiSuccess {string} message Message stating the new user has been created successfully
 *
 * @apiSuccessExample Success-Response: Create a new User
 * HTTP/1.1 201 OK
 * {
 *     "message":"Successfully created new user."
 * }
 *
 * @apiError (Client Error) {400} NotAllMandatoryFields The request did not contain all mandatory fields
 * @apiError (Client Error) {400} CouldNotInsertTheRole The request did not give a role to a new user
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 400 Bad Request
 * {
 *     "message":"Database request failed."
 * }
 *
 * @apiErrorExample CouldNotInsertTheRole:
 * HTTP/1.1 400 Bad Request
 * {
 *     "message":"Could not insert the role"
 * }
 */
app.post('/user', (req: Request, res: Response) => {
    // Read data from request body (valid for registration and userManager)
    const vorname: string = req.body.vorname;
    const nachname: string = req.body.nachname;
    const nutzername: string = req.body.nutzername;
    const passwort: string = req.body.passwort;
    const email: string = req.body.email;
    const geburtsdatum: string = req.body.geburtsdatum;
    const strasse: string = req.body.strasse;
    const plz: number = req.body.plz;
    const wohnort: string = req.body.wohnort;
    const profilbild: string = req.body.profilbild;



    // Add new user if first and last name exist
    if (vorname && nachname) {
        // Array with data transfered from ajax request
        let data: [string, string, string, string, string, string, string, number, string, string] = [
            vorname,
            nachname,
            nutzername,
            passwort,
            email,
            geburtsdatum,
            strasse,
            plz,
            wohnort,
            profilbild
        ];
        // // Create query with all user dates
        let query: string = 'INSERT INTO benutzer (vorname, nachname, nutzername, passwort, email, geburtsdatum, strasse, plz, wohnort, profilbild) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);';
        // Execute database query
        database.query(query, data, (innerErr: MysqlError, result: any) => {
            // Database operation has failed
            if (innerErr) {
                //error message
                res.status(400).send({
                    message: 'Database request failed.' + innerErr
                })
            } else {
                let innerQuery: string = 'INSERT INTO `gruppenzuteilung` (`benutzerId`, `benutzergruppenId`) VALUES ?';
                const innerData = [];

                innerData.push([result.insertId, 1])

                database.query(innerQuery, [innerData], (innerErr: MysqlError) => {
                    if (innerErr) {
                        console.log(result);
                        res.status(400).send({
                            message: "Could not insert the role"
                        })
                    }
                })
                const innerinnerData=[];
                innerinnerData.push([result.insertId]);
                let innerinnerQuery: string ="INSERT INTO `warenkorb` (`warenkorbId`) VALUES (?);"

                database.query(innerinnerQuery, [innerinnerData], (innerErr: MysqlError) => {
                    if (innerErr) {
                        res.status(400).send({
                            message: "Could not create a shoppingcart"
                        })
                    }
                })
                const innerinnerinnerData=[result.insertId, result.insertId];
                console.log(innerinnerinnerData)

                let innerinnerinnerQuery: string ="INSERT INTO `besitzt` (`warenkorbId`, `benutzerId`) VALUES (?, ?)";

                database.query(innerinnerinnerQuery, innerinnerinnerData, (innerErr: MysqlError) => {
                    if (innerErr) {
                        res.status(400).send({
                            message: "Could not link the shoppingcart to the user"
                        })
                    }
                })
                /*//success message
                res.status(201).send({
                    message: 'Successfully created new user.'
                })*/
            }

        });
    }
});

//get route to get one special user, data transfered in URL
/**
 * @api {get} /user/:userId Get user with given id
 * @apiName getUser
 * @apiGroup User
 * @apiVersion 2.0.0
 *
 * @apiParam {number} userId The id of the requested user
 *
 * @apiSuccess {User} user The requested user object
 * @apiSuccess {string} message Message stating the user has been found
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "user":{
 *         "id":1,
 *         "vorname":"Peter",
 *         "nachname":"Kneisel",
 *         "gruppenzuteilung":"",
 *         "benutzergruppe":"Festivalbesucher"
 *     },
 *     "message":"user found"
 * }
 *
 * @apiError (Client Error) {500} NotAllMandatoryFields The request did not contain all mandatory fields
 * @apiError (Client Error) {404} NotFound The requested user can not be found
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"Database request failed" + err
 * }
 *
 * @apiErrorExample NotFound:
 * HTTP/1.1 404 Not Found
 * {
 *     "message":"User not found"
 * }
 */
app.get('/user/:userId', (req: Request, res: Response) => {
    // Read data from request parameters
    const userId: number = Number(req.params.userId);
    //to check if user is selected correctly
    console.log(userId);
    //userId gets transfered to database, userdata selected in SQL string
    let data: number = userId;
    let query: string = "SELECT benutzer.benutzerId, benutzer.vorname, benutzer.nachname, benutzer.nutzername, benutzer.passwort, benutzer.email, benutzer.geburtsdatum, benutzer.strasse, benutzer.plz, benutzer.wohnort, gruppenzuteilung.*, benutzergruppe.beschreibung FROM benutzer LEFT JOIN gruppenzuteilung ON gruppenzuteilung.benutzerId = benutzer.benutzerId LEFT JOIN benutzergruppe ON gruppenzuteilung.benutzergruppenId = benutzergruppe.benutzergruppenId WHERE benutzer.benutzerId = ?";
    database.query(query, data, (err: MysqlError, rows: any) => {
        if (err) {
            //error message
            res.status(500).send({
                message: "Database request failed" + err
            })
        } else {
            //if exactly one user is selected
            if (rows.length === 1) {
                res.status(200).send({
                    //success message
                    message: "user found",
                    user: rows
                });
                //if construct gets leaved
                return;
            }
            res.status(404).send({
                //error message
                message: "User not found"
            })
        }
    });
});

//put route to update user
/**
 * @api {put} /user/:userId Update user with given id
 * @apiName putUser
 * @apiGroup User
 * @apiVersion 2.0.0
 *
 * @apiParam {number} userId The id of the requested user
 * @apiParam {string} firstName The (new) first name of the user
 * @apiParam {string} lastName The (new) last name of the user
 * @apiParam {string} username The (new) username of the user
 * @apiParam {string} password The (new) password of the user
 * @apiParam {string} e-mail The (new) e-mail of the user
 * @apiParam {string} birthdate The (new) birthdate of the user
 * @apiParam {string} street The (new) street of the user
 * @apiParam {string} plz The (new) plz of the user
 * @apiParam {string} place The (new) place of the user
 *
 * @apiSuccess {string} message Message stating the user has been updated
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "message":"Successfully updated user ..."
 * }
 *
 * @apiError (Client Error) {500} NotAllMandatoryFields The request did not contain all mandatory fields
 * @apiError (Client Error) {404} NotFound The requested user can not be found
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"Database request failed" + err
 * }
 *
 * @apiErrorExample NotFound:
 * HTTP/1.1 404 Not Found
 * {
 *     "message":"Updating the user failed"
 * }
 */
app.put('/user/:userId', (req: Request, res: Response) => {
    // Read data from request parameters and body
    const userId: number = Number(req.params.userId);
    const firstName: string = req.body.vorname;
    const lastName: string = req.body.nachname;
    const userName: string = req.body.nutzername;
    const password: string = req.body.passwort;
    const email: string = req.body.email;
    const geburtsdatum: string = req.body.geburtsdatum;
    const street: string = req.body.strasse;
    const plz: number = req.body.plz;
    const place: string = req.body.wohnort;

    let data: [string, string, number, string, string, string, string, string, number, string] = [firstName, lastName, userId, userName, password, email, geburtsdatum, street, plz, place];

    //sql string to replace values in database
    let query: string = 'UPDATE `benutzer` SET `vorname` = ?, `nachname` = ?, `benutzerId`=?, `nutzername` = ?, `passwort` = ?, `email` = ?, `geburtsdatum` = ?, `strasse` = ?, `plz` = ?, `wohnort` = ? WHERE `benutzerId` =' + userId;
    database.query(query, data, (err: MysqlError, result: any) => {
        if (err) {
            res.status(500).send({
                //error message
                message: "Database request failed" + err
            });
        } else {
            //if exactly one user is selected
            if (result.affectedRows === 1) {
                res.status(200).send({
                    //success message
                    message: "Succesfully updated user" + userId
                });
            } else {
                //error message
                res.status(404).send({
                    message: "Updating the user failed"
                });
            }
        }
    });
});

// Updates datas from User in Userprofile
/**
 * @api {get} /userProfile/:userId Updates User in Userprofile
 * @apiName getUserProfile
 * @apiGroup userProfile
 * @apiVersion 2.0.0
 *
 * @apiParam {number} userId of the user
 * @apiParam {string} firstname of the user
 * @apiParam {string} lastname of the user
 * @apiParam {string} username of the user
 * @apiParam {string} password of the user
 * @apiParam {string} email of the user
 * @apiParam {string} birthdate of the user
 * @apiParam {string} street of the user
 * @apiParam {number} plz of the user
 * @apiParam {string} place of the user
 *
 * @apiSuccess {string} Update the User with the Value ?
 * @apiSuccess {string} message Message stating the users have been found
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "userList": [
 *      {
 *        "userId": "1",
 *        "vorname": "Hans",
 *        "nachname": "Mustermann",
 *        "username": "mmustermmann",
 *        "password": "mmustermann",
 *        "email": "m.mustermann@emai.com",
 *        "birthdate": "11.11.2011",
 *        "street": "mmustermannstrasse",
 *        "plz": "111111",
 *        "place": "musterort",
 *     },
 *    ]
 *    "message":"Successfully requested user list"
 * }
 *
 * @apiError (Client Error) {500} NotAllMandatoryFields The request did not contain all mandatory fields
 * @apiError (Client Error) {404} NotFound The requested user can not be found
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"Database request failed" + err
 * }
 *
 * @apiErrorExample NotFound:
 * HTTP/1.1 404 Not Found
 * {
 *     "message":"Updating the user failed"
 * }
 */
app.put('/userProfil', (req: Request, res: Response) => {

    // Read data from request parameters and body
    const userId: number = (Number(req.session.userId));
    const firstName: string = req.body.vorname;
    const lastName: string = req.body.nachname;
    const userName: string = req.body.nutzername;
    const password: string = req.body.passwort;
    const email: string = req.body.email;
    const birthdate: number = req.body.geburtsdatum;
    const street: string = req.body.strasse;
    const plz: number = req.body.plz;
    const place: string = req.body.wohnort;

    let data: [string, string, string, string, string, number, string, number, string] = [firstName, lastName, userName, password, email, birthdate, street, plz, place];

    //sql string to replace values in database
    let query: string = 'UPDATE `benutzer` SET `vorname` = ?, `nachname` = ?, `nutzername` = ?, `passwort` = ?, `email` = ?, `geburtsdatum` = ?, `strasse` = ?, `plz` = ?, `wohnort` = ? WHERE `benutzer`.`benutzerId` = ' + userId;
    database.query(query, data, (err: MysqlError, result: any) => {
        if (err) {
            res.status(500).send({
                //error message
                message: "Database request failed" + err
            });
        } else {
            //if exactly one user is selected
            if (result.affectedRows === 1) {
                res.status(200).send({
                    //success message
                    message: "Succesfully updated user"
                });
            } else {
                //error message
                res.status(404).send({
                    message: "Updating the user failed"
                });
            }
        }
    });
});

//put route updates menge in beinhaltet
/**
 * @api {put} /updateTicketMenge/:ticketId Updates Menge in beinhaltet
 * @apiName putupdateTicketMenge
 * @apiGroup updateTicketMenge
 * @apiVersion 2.0.0
 *
 * @apiParam {number} tiketId The id of the requested ticket
 * @apiParam {number} menge Shows the menge in warenkorb
 * @apiParam {number} userId The id of the requested user
 *
 * @apiSuccess {string} message Message stating the user has been updated
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "message":"Successfully updated menge"
 * }
 *
 * @apiError (Client Error) {500} NotAllMandatoryFields The request did not contain all mandatory fields
 * @apiError (Client Error) {404} NotFound The requested menge can not be found
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"Database request failed" + err
 * }
 *
 * @apiErrorExample NotFound:
 * HTTP/1.1 404 Not Found
 * {
 *     "message":"Updating menge failed"
 * }
 */
app.put('/updateTicketMenge/:ticketId', (req: Request, res: Response) => {
    // Read data from request parameters and body
    const ticketId: number = Number(req.params.ticketId);
    const menge: number = Number(req.body.menge);
    const userId: number = req.session.userId; //User Id is the same as the Warenkorb Id

    let data: [number, number, number] = [menge, ticketId, userId];

    //sql string to replace values in database
    let query: string = "UPDATE `beinhaltet` SET `menge`= ? WHERE `ticketId` = ? AND `warenkorbId` = ?";

    database.query(query, data, (err: MysqlError, result: any) => {
        if (err) {
            res.status(500).send({
                //error message
                message: "Database request failed" + err
            });
        } else {
            //if exactly one user is selected
            if (result.affectedRows === 1) {
                res.status(200).send({
                    //success message
                    message: "Succesfully updated Menge"
                });
            } else {
                //error message
                res.status(404).send({
                    message: "Updating Menge failed"
                });
            }
        }
    });
});

//delete route to delete user
/**
 * @api {delete} /user/:userId Delete user with given id
 * @apiName deleteUser
 * @apiGroup User
 * @apiVersion 2.0.0
 *
 * @apiParam {number} userId The id of the requested user
 *
 * @apiSuccess {string} message Message stating the user has been deleted
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "message":"Successfully deleted user" + userid
 * }
 *
 * @apiError (Client Error) {500} NotAllMandatoryFields The request did not contain all mandatory fields
 * @apiError (Client Error) {404} NotFound The requested user can not be found
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"Database request failed" + err
 * }
 *
 * @apiErrorExample NotFound:
 * HTTP/1.1 404 Not Found
 * {
 *     "message":"The requested user can not be deleted"
 * }
 */
app.delete('/user/:userId', (req: Request, res: Response) => {
    // Read data from request parameters
    const userId: number = Number(req.params.userId);
console.log(userId);
    let data: number = userId;
    //user with chosen id gets deleted
    let query: string = "DELETE FROM `benutzer` WHERE `benutzer`.`benutzerId` = ?;";

    database.query(query, data, (err: MysqlError, result: any) => {
        if (err) {
            res.status(500).send({
                message: "Database request failed" + err
            });
        } else {
            if (result.affectedRows === 1) {
                res.status(200).send({
                    message: "succesfully deleted user" + userId
                });
            } else {
                res.status(404).send({
                    message: "The requested user can not be deleted"
                })
            }
        }
    })
});

//Delete ticket in shopping cart
/**
 * @api {delete} /deleteTicket/:ticketId Deletes a ticket in the shopping cart
 * @apiName deleteTicket
 * @apiGroup Ticket
 * @apiVersion 2.0.0
 *
 * @apiParam {number} ticketId The id of the requested ticket
 * @apiParam {number} userId The id of the requested user
 *
 * @apiSuccess {string} message Message stating the user has been deleted
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "message":"Successfully deleted ticket" + ticketId
 * }
 *
 * @apiError (Client Error) {500} NotAllMandatoryFields The request did not contain all mandatory fields
 * @apiError (Client Error) {404} NotFound The requested ticket can not be found
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 500 Bad Request
 * {
 *     "message":"Database request failed" + err
 * }
 *
 * @apiErrorExample NotFound:
 * HTTP/1.1 404 Not Found
 * {
 *     "message":"The requested ticket can not be deleted"
 * }
 */
app.delete('/deleteTicket/:ticketId', (req: Request, res: Response) => {
    // Read data from request parameters
    const ticketId: number = Number(req.params.ticketId);
    const userId: number = req.session.userId; //User Id is the same as the warenkorbId

    let data: [number, number] = [userId, ticketId];
    //ticket with chosen id gets deleted

    let query: string = 'DELETE FROM `beinhaltet` WHERE `warenkorbId`= ? AND `ticketId` = ?';

    database.query(query, data, (err: MysqlError, result: any) => {
        if (err) {
            res.status(500).send({
                message: "Database request failed" + err
            });
        } else {
            if (result.affectedRows === 1) {
                res.status(200).send({
                    message: "succesfully deleted ticket" + ticketId
                });
            } else {
                res.status(404).send({
                    message: "The requested ticket can not be deleted"
                })
            }
        }
    })
});