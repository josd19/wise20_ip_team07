/*****************************************************************************
 * Import package                                                            *
 *****************************************************************************/
import express = require ('express');
import { Request, Response } from 'express';
import {Group, User} from '../model/user';
import mysql = require('mysql');
import { Connection, MysqlError } from 'mysql';
import session = require('express-session');



/*****************************************************************************
 * Define global variables                                                   *
 *****************************************************************************/


/*****************************************************************************
 * Define and start web-app server, define json-Parser                       *
 *****************************************************************************/
const app = express();
app.listen(8080, () => {
    console.log('Server started: http://localhost:8080');
});
const database : Connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'userman'
});
app.use(session({
    resave: true,
    saveUninitialized: true,
    rolling: true,
    secret: "geheim",
    name: "mySessionCookie",
    cookie: {maxAge: 5*60*1000}
}));

app.use(express.json());

database.connect((err: MysqlError) => {
    if (err) {
        console.log('Database connection failed: ', err);
    } else {
        console.log('Database is connected');
    }
});
declare module 'express-session'{
    interface SessionData {
        username: string
    }
}
/*****************************************************************************
 * STATIC ROUTES                                                             *
 *****************************************************************************/
const basedir: string = __dirname + '/../..';  // get rid of /server/src
app.use('/', express.static(basedir + '/client/views'));
app.use('/css', express.static(basedir + '/client/css'));
app.use('/src', express.static(basedir + '/client/src'));
app.use('/jquery', express.static(basedir + '/client/node_modules/jquery/dist'));
app.use('/popperjs', express.static(basedir + '/client/node_modules/popper.js/dist'));
app.use('/bootstrap', express.static(basedir + '/client/node_modules/bootstrap/dist'));
app.use('/font-awesome', express.static(basedir + '/client/node_modules/font-awesome/css'));

/*****************************************************************************
 * HTTP ROUTES: USER, USERS                                                  *
 *****************************************************************************/
function isLoggedIn(){
    return(req: Request, res:Response, next)=>{
        if (req.session.username){
            next();
        }else{
            res.status(401).send({
                message:"Session has expired"
            })
        }
    }
}
app.get('/login', isLoggedIn(),(req:Request, res:Response)=>{
    res.status(200).send({
        message:"User " + req.session.username + " still logged in",
        user: req.session.username
    })
});
app.post('/logout', (req: Request, res: Response)=>{
    delete req.session.username;
    res.status(200).send({
        message: "Successfully logged out"
    })
});
app.post('/login', (req: Request, res: Response) => {
    const username: string = req.body.loginname;
    const password: string = req.body.password;

        const data: [string, string] = [
            username,
            password,
        ];

        const query = ('SELECT * FROM userlist WHERE username = ? AND password = ?');
        database.query(query, data, (err: MysqlError, rows: any) => {

            if (err) {
                res.status(500).send({
                    message: "There was an error"
                });
            } else {
                if (rows.length===1) {
                  req.session.username=username;


                    res.status(200).send({
                        message: "Logged in as: "+ req.session.username,
                        username
                    });
                }else{
                    res.status(401).send({
                        message:"Wrong password or username"
                    })
                }
            }
        })
});



/**
 * @api {post} /user Create a new user
 * @apiName postUser
 * @apiGroup User
 * @apiVersion 2.0.0
 *
 * @apiParam {string} firstName First name of the user
 * @apiParam {string} lastName Last name of the user
 *
 * @apiSuccess {string} message Message stating the new user has been created successfully
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "message":"Successfully created new user"
 * }
 *
 * @apiError (Client Error) {400} NotAllMandatoryFields The request did not contain all mandatory fields
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 400 Bad Request
 * {
 *     "message":"Not all mandatory fields are filled in"
 * }
 */
app.post('/user', isLoggedIn(), (req: Request, res: Response) => {
    // Read data from request body
    const firstName: string = req.body.firstName;
    const lastName: string = req.body.lastName;
    let userGroups: Number [] = req.body.groups;
    // add a new user if first- and lastname exist
    if (firstName && lastName) {
        // Create new user
        let data:[string, string, string]=[
            new Date().toLocaleString(),
            firstName,
            lastName
        ];
        let query: string = "INSERT INTO userlist (creationTime, firstName, lastName) VALUES (?, ?, ?);";
        // Add user to user list
        database.query(query, data, (err: MysqlError, result: any)=>{
            if (err || result === null) {
                res.status(400).send({
                    message:"There was an error"
                });
            } else {
                let innerQuery: string =
                    'INSERT INTO `user_group` (`user_id`, `group_id`) VALUES ?';
                const innerData = [];
                for (const group of userGroups) {
                    innerData.push([result.insertId, group])
                }
                database.query(innerQuery, [innerData], (innerErr: MysqlError)=>{
                    if (innerErr){
                        console.log(result);
                        res.status(400).send({
                            message:"Could not insert the role"
                        })
                    } else {
                        res.status(201).send({
                            message: 'Successfully created new user',
                        });
                    }
                });
            }
        });
    } else {
        res.status(400).send({
            message: 'Not all mandatory fields are filled in',
        });
    }
});

/**
 * @api {get} /user:userId Get user with given id
 * @apiName getUser
 * @apiGroup User
 * @apiVersion 2.0.0
 *
 * @apiParam {number} userId The id of the requested user
 *
 * @apiSuccess {User} user The requested user object
 * @apiSuccess {string} message Message stating the user has been found
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "user":{
 *         "id":1,
 *         "firstName":"Peter",
 *         "lastName":"Kneisel",
 *         "creationTime":"2018-10-21 14:19:12"
 *     },
 *     "message":"Successfully got user"
 * }
 *
 *  @apiError (Client Error) {404} NotFound The requested user can not be found
 *
 * @apiErrorExample NotFound:
 * HTTP/1.1 404 Not Found
 * {
 *     "message":"The requested user can not be found."
 * }
 */
app.get('/user/:userId', isLoggedIn(), (req: Request, res: Response) => {
    // Read data from request parameters
    const userId: number = Number(req.params.userId);
    console.log(userId);
    let data: number = userId;
    let query: string = "SELECT userlist.id AS 'userid', userlist.firstName, userlist.lastName, grouplist.id AS 'groupid', grouplist.name AS 'groupname' FROM `userlist` JOIN user_group ON userlist.id = user_group.user_id JOIN grouplist ON user_group.group_id = grouplist.id WHERE userlist.id = ? ORDER BY userlist.id";

    database.query(query, data, (err:MysqlError, rows: any)=>{
        if (err){
            res.status(500).send({
                message: "Database request failed" + err
            })
        }else {
                if (rows.length===1) {
                    res.status(200).send({
                        message: "user found",
                        user: rows
                    });
                    return;
                }
            res.status(404).send({
                message:"User not found"
            })
        }
    });
});

/**
 * @api {put} /user/:userId Update user with given id
 * @apiName putUser
 * @apiGroup User
 * @apiVersion 2.0.0
 *
 * @apiParam {number} userId The id of the requested user
 * @apiParam {string} firstName The (new) first name of the user
 * @apiParam {string} lastName The (new) last name of the user
 *
 * @apiSuccess {string} message Message stating the user has been updated
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "message":"Successfully updated user ..."
 * }
 *
 * @apiError (Client Error) {400} NotAllMandatoryFields The request did not contain all mandatory fields
 * @apiError (Client Error) {404} NotFound The requested user can not be found
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 400 Bad Request
 * {
 *     "message":"Not all mandatory fields are filled in"
 * }
 *
 * @apiErrorExample NotFound:
 * HTTP/1.1 404 Not Found
 * {
 *     "message":"The user to update could not be found"
 * }
 */
app.put('/user/:userId', isLoggedIn(), (req: Request, res: Response) => {
    // Read data from request
    const userId: number = Number(req.params.userId);
    const firstName: string = req.body.firstName;
    const lastName: string = req.body.lastName;

    console.log(userId);
    let data:[string, string, number] = [firstName, lastName, userId];
    let query: string = "UPDATE `userlist` SET `firstName` = ?, `lastName` = ? WHERE `userlist`.`id` = ?";

    database.query(query, data, (err: MysqlError, result: any) => {
        if (err){
            res.status(500).send({
                message:"Database request failed" + err
            });
        }else{
            if (result.affectedRows === 1){
                console.log("beabrietet" + userId);
                res.status(200).send({
                    message:"Succesfully updated user" + userId
                });
            }else{
                res.status(404).send({
                    message:"Updating the user failed"
                });
            }
        }
    });
});

/**
 * @api {delete} /user/:userId Delete user with given id
 * @apiName deleteUser
 * @apiGroup User
 * @apiVersion 2.0.0
 *
 * @apiParam {number} userId The id of the requested user
 *
 * @apiSuccess {string} message Message stating the user has been updated
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "message":"Successfully deleted user ..."
 * }
 */
app.delete('/user/:userId', isLoggedIn(), (req: Request, res: Response) => {
    // Read data from request
    const userId: number = Number(req.params.userId);
    // delete user
    let data: number = userId;
    let query: string = "DELETE FROM `userlist` WHERE `userlist`.`id` = ?;";

    database.query(query, data, (err:MysqlError, result:any) => {
        if (err) {
            res.status(500).send({
                message:"Database request failed"+ err
            });
        }else {
            if (result.affectedRows === 1){
                res.status(200).send({
                    message: "succesfully deleted user"+userId
                });
            } else {
                res.status(404).send({
                    message:"The requested user can not be deleted"
                })
            }
        }
    })
});

/**
 * @api {get} /users Get all users
 * @apiName getUsers
 * @apiGroup Users
 * @apiVersion 2.0.0
 *
 * @apiSuccess {User[]} userList The list of all users
 * @apiSuccess {string} message Message stating the users have been found
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "userList": [
 *      {
 *        "firstName": "Hans",
 *        "lastName": "Mustermann",
 *        "creationTime": "2018-11-04T13:02:44.791Z",
 *        "id": 1
 *     },
 *      {
 *        "firstName": "Bruce",
 *        "lastName": "Wayne",
 *        "creationTime": "2018-11-04T13:03:18.477Z",
 *        "id": 2
 *      }
 *    ]
 *    "message":"Successfully requested user list"
 * }
 */
app.get('/user' ,(req: Request, res: Response) => {
    // Send user list to client
    let data: number = Number(req.params.userId);
    let query: string = "SELECT userlist.id AS 'userid', userlist.firstName, userlist.lastName, grouplist.id AS 'groupid', grouplist.name AS 'groupname' FROM `userlist` JOIN user_group ON userlist.id = user_group.user_id JOIN grouplist ON user_group.group_id = grouplist.id ORDER BY userlist.id";

    database.query(query, data, (err:MysqlError, rows: any)=>{
        if (err){
          res.status(500).send({
              message: "Database request failed" + err
          });
        }else {
            const userList:User[]=[];
            let lastUser:User;
            for(const row of rows) {
                let groupId = row.groupid;
                let name = row.groupname;
                if (!lastUser || lastUser.id !== row.userid) {
                    lastUser = new User(
                        row.userid,
                        row.firstName,
                        row.lastName,
                        new Date(row.creationTime),
                    );
                    userList.push(lastUser);
                }
                lastUser.groups.push(new Group(groupId, name))
            }
            res.status(200).send({
                userList:userList,
                message:"Succesfully requested user list"
            });

        }
    });
});

app.get('/groups', (req: Request, res: Response) => {
    const query: string = 'SELECT * FROM `grouplist`';
    database.query(query, (err: MysqlError, rows: any) =>{
        if (err){
            res.status(500).send({
                message:"Database request failed: " + err
            });
        }
        else {
            res.status(200).send({
                groups:rows,
                message: "Succesfully requested groups list"
            })
        }
    })
});

