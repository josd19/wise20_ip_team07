-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 15. Jan 2021 um 09:52
-- Server-Version: 10.4.6-MariaDB
-- PHP-Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `userman`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `grouplist`
--

CREATE TABLE `grouplist` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE latin1_german1_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

--
-- Daten für Tabelle `grouplist`
--

INSERT INTO `grouplist` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'User');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `userlist`
--

CREATE TABLE `userlist` (
  `id` int(11) NOT NULL,
  `firstName` varchar(255) COLLATE latin1_german1_ci NOT NULL,
  `lastName` varchar(255) COLLATE latin1_german1_ci NOT NULL,
  `creationTime` varchar(255) COLLATE latin1_german1_ci NOT NULL,
  `username` varchar(255) COLLATE latin1_german1_ci NOT NULL,
  `password` varchar(255) COLLATE latin1_german1_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

--
-- Daten für Tabelle `userlist`
--

INSERT INTO `userlist` (`id`, `firstName`, `lastName`, `creationTime`, `username`, `password`) VALUES
(26, 'Seppi', 'Waldschmidt', '2020-11-28 16:00:52', 'seppi', '1234'),
(27, 'Peter', 'Kneisel', '2020-11-28 16:01:06', '', ''),
(28, 'Wolf', 'Rost', '2020-11-28 16:01:17', '', ''),
(29, 'Dennis', 'Priefer', '2020-11-28 16:01:32', '', ''),
(43, 'admin', 'admin', '2020-12-2 19:28:04', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_group`
--

CREATE TABLE `user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

--
-- Daten für Tabelle `user_group`
--

INSERT INTO `user_group` (`user_id`, `group_id`) VALUES
(26, 1),
(27, 2),
(28, 2),
(29, 2),
(43, 1);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `grouplist`
--
ALTER TABLE `grouplist`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `userlist`
--
ALTER TABLE `userlist`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `user_group`
--
ALTER TABLE `user_group`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `group_id` (`group_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `userlist`
--
ALTER TABLE `userlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `user_group`
--
ALTER TABLE `user_group`
  ADD CONSTRAINT `user_group_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `userlist` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_group_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `grouplist` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
