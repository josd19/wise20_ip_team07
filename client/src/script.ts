/*****************************************************************************
 * interface declaration                                                     *
 *****************************************************************************/
// Interface representing a user
interface User {
    benutzerId: number;
    vorname: string;
    nachname: string;
    nutzername: string;
    username: string;
    profilbild: string;
    passwort: string;
    email: string;
    geburtsdatum: string;
    strasse: string;
    plz: number;
    wohnort: string;
}

// Interface representing a Group
interface Group {
    benutzergruppenId: number;
    beschreibung: string;
}

// Interface representing a Ticket
interface Ticket {
    ticketId: number;
    preis: number;
    beschreibung: string;
}

// Interface representing a Festival
interface Festival {
    festivalId: number;
    name: string;
    stages: string;
    beschreibung: string;
    festivalkartenbild: string;
    genre: string;
    datum: string;
    bild: string;
    href: string;
    startdatum: string;
    mindestpreis: string;
}

// Interface representing a Warenkorb
interface Warenkorb {
    beschreibung: string;
    preis: number;
    ticketId: number;
    menge: number;
}

let searchFestivalarray: any [] = [];
let sortFestivalarray: any [] = [];
let userArray: any [] = [];


/*****************************************************************************
 * Event Handlers (callbacks)                                                *
 *****************************************************************************/
//Ajax Request for login
//Handler, when clicking on Login-Button
function loginfunction(event) {
    event.preventDefault();

    const loginnameinput: JQuery = $('#username');
    const passwordinput: JQuery = $('#password');

    const loginname: string = loginnameinput.val().toString().trim();
    const password: string = passwordinput.val().toString().trim();

    if (loginname && password) {
        $.ajax({
            url: "http://localhost:8080/login",
            type: "POST",                                                 // POST request
            dataType: "json",                                             // expection json in response
            data: JSON.stringify({
                loginname,
                password,
            }),
            contentType: 'application/json',                            // using json in request

            success: (response) => {
                $('#username').val("").hide();         // hide username, password and Btn
                $('#password').val("").hide();
                $('#loginmodalbtn').hide();
                $('#dropdown_Profil').show();                     // show Dropdown, Logout-Btn
                $('#btnLogout').show();
                $('#loggedInAsUser').append(response.username); //username gets showed in dropdown
                if (response.role === "Admin") {                         // if role Admin show Usermanager-Btn
                    $('#openUsermanagerbtn').show();
                }

                getUsers()                                      // get die Userlist from database
                renderMessage(response.message);                // render the message
            },
            error: (jqXHRresponse) => {
                renderMessage(jqXHRresponse.responseJSON.message);
            },
        });
    } else {
        renderMessage('Not all fields are filled. Please check the Login.');
    }
}

//Check, if user is already logged in (e.g. after a refresh of website)
function checkLogin() {
    $.ajax({
        url: 'http://localhost:8080/login',
        type: 'GET',                            // GET request to check login and get user
        dataType: 'json',                       // expection json

        success: (response) => {
            $('#loginmodalbtn').hide();
            $('#username').hide();
            $('#password').hide();
            $('#dropdown_Profil').show()
            $('#loggedInAsUser').append(" " + response.profile);
            if (response.role === "Admin") {
                $('#openUsermanagerbtn').show();
            };
            if (response.role !== "Admin") {
                $('#openUsermanagerbtn').hide();
            };
            if (response.role === "Admin") {
                getCurrentAdmin();
            } else {
                getCurrentUser();
            };
            if (response.role !== "Admin") {
                getCurrentUser();
            } else {
                getCurrentAdmin();
            }

            getUsers();
            getShoppingCart();
            renderMessage(response.message);
            watchStream();

        },
        error: (jqXHRresponse) => {
            $('#loginmodalbtn').show();
            $('#btnLogout').hide();
            $('#hintForLogin').show();
            $('#dropdown_Profil').hide()
            renderMessage(jqXHRresponse.responseJSON.message); // render Message
            $('#openUsermanagerbtn').hide();
        },
    })
}

//Handler, when clicking on Logout-Button
function logoutfunction() {
    $.ajax({
        url: "http://localhost:8080/logout",
        type: "POST",                                       // POST route
        dataType: "json",                                   // expecting json
        data: JSON.stringify({}),
        contentType: 'application/json',

        success: (response) => {
            $('#username').val("").show();
            $('#password').val("").show();
            $('#loginmodalbtn').show();
            $('#btnLogout').hide();
            $('#loggedInAsUser').empty()
            $('#dropdown_Profil').hide()
            $('#user-table-body').hide()
            $('#openUsermanagerbtn').hide();

            renderMessage(response.message);            // render Message
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    });
}

// Function for getting all festivals from database
function getFestivals() {

    $.ajax({
        url: '/alleFestivals',
        type: 'GET',                            // GET route
        dataType: 'json',                       // expetion json
        success: (response) => {
            renderFestivalList(response.festivalList);
            renderProfileFestivals(response.festivalList)
            sortFestivalarray = response.festivalList;
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}

// Function for delete a ticket from database
function deleteTicket(event) {
    const ticketId: number = $(event.currentTarget).data('ticket-id');

    $.ajax({
        url: 'http://localhost:8080/deleteTicket/' + ticketId,
        type: 'DELETE',                         // DELETE route
        dataType: 'json',                       // expection json
        success: (response) => {
            // render Message
            renderMessage(response.message);
            // Get new user list from server
            getShoppingCart();
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
};

// Function to get the GenreValue from Dropdown
function getGenre() {
    let genrevalue: any = $('#select-genre').val();
    //console.log(genrevalue);
    // different cases to get a specific genre
    switch (genrevalue) {
        case "Rock":
            getRockFestivals();
            break;
        case "Pop":
            getPopFestivals();
            break;
        case "Techno":
            getTechnoFestivals();
            break;
        // "alle Genre" = reload the page to get all festivals
        default:
            location.reload();
    }
}

// Function for getting all festivals from database with the genre ROCK
function getRockFestivals() {

    $.ajax({
        url: '/rockfestivals',
        type: 'GET',                            // GET route
        dataType: 'json',                       // expetion json
        success: (response) => {
            renderFestivalList(response.festivalList);
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}

// Function for getting all festivals from database with the genre POP
function getPopFestivals() {

    $.ajax({
        url: '/popfestivals',
        type: 'GET',                            // GET route
        dataType: 'json',                       // expetion json
        success: (response) => {
            renderFestivalList(response.festivalList);
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}

// Function for getting all festivals from database with the genre TECHNO
function getTechnoFestivals() {

    $.ajax({
        url: '/technofestivals',
        type: 'GET',                            // GET route
        dataType: 'json',                       // expetion json
        success: (response) => {
            renderFestivalList(response.festivalList);
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}

// Function to reset the Genre filter and reload the page
function resetGenreFilter() {
    location.reload();
}

// Function for getting current user from database
function getCurrentUser() {
    // Perform ajax request to update local user list
    $.ajax({
        url: '/userProfil',
        type: 'GET',                            // GET route
        dataType: 'json',                       // expetion json

        success: (response) => {
            renderUserProfil(response.userList);
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}

// Function for getting current admin from database
function getCurrentAdmin() {
    // Perform ajax request to update local user list
    $.ajax({
        url: '/userProfil',
        type: 'GET',                            // GET route
        dataType: 'json',                       // expetion json

        success: (response) => {
            renderAdminProfil(response.userList);
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}

// Function for getting all users from database
function getUsers() {
    // Perform ajax request to update local user list
    $.ajax({
        url: '/benutzer',
        type: 'GET',                            // GET route
        dataType: 'json',                       // expetion json

        success: (response) => {
            renderUserList(response.userList);
            userArray=response.userList;
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}

// Function for getting all groups from database
function getGroups() {
    // Perform ajax request to update local user list
    $.ajax({
        url: '/groups',
        type: 'GET',                            // GET route
        dataType: 'json',                       // expetion json

        success: (response) => {
            renderGroupList(response.groupList);
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}

// Function for add Ticket into Shopping Cart (adds new Item into the database)
function addIntoShoppingCart(event) {

    const ticketId: number = $(event.currentTarget).data('ticket-id');

    $.ajax({
        url: 'http://localhost:8080/addToCart/' + ticketId,
        type: 'POST',                         // DELETE route
        dataType: 'json',                       // expection json
        success: (response) => {
            // render Message
            renderMessage(response.message);
            // Get new user list from server
            getShoppingCart();
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}

// Function for getting all Tickets in Shopping Cart (get the item from the databse)
function getShoppingCart() { //Aufgerufen in CheckLogin

    $.ajax({
        url: '/warenkorb',
        type: 'GET',                            // GET route
        dataType: 'json',                       // expetion json
        success: (response) => {
            renderShoppingCart(response.shoppingList);
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}

// Function for getting all Tickets on detail page (Buchungspakete)
function getTickets() {

    $.ajax({
        url: '/alleTickets',
        type: 'GET',                            // GET route
        dataType: 'json',                       // expetion json
        success: (response) => {
            renderTicketList(response.ticketList);
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}

// Function for adding a user to database
function addUser(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();

    // Define JQuery HTML objects
    const addUserForm: JQuery = $('#add-user-form');
    //const firstNameField: JQuery = $('#add-first-name-input');
    //const lastNameField: JQuery = $('#add-last-name-input');
    const vornameInput: JQuery = $("#vorname");
    const nachnameInput: JQuery = $("#nachname");
    const nutzernameInput: JQuery = $("#nutzername");
    const passwortInput: JQuery = $("#passwort");
    const emailInput: JQuery = $("#email");
    const geburtsdatumInput: JQuery = $("#geburtsdatum");
    const strasseInput: JQuery = $("#strasse");
    const plzInput: JQuery = $("#plz");
    const wohnortInput: JQuery = $("#wohnort");

    const userGroups: any = [];
    // Read values from input fields
    // const vorname: string = firstNameField.val().toString().trim();
    // const nachname: string = lastNameField.val().toString().trim();
    const vorname: string = vornameInput.val().toString().trim();
    const nachname: string = nachnameInput.val().toString().trim();
    const nutzername: string = nutzernameInput.val().toString().trim();
    const passwort: string = passwortInput.val().toString().trim();
    const email: string = emailInput.val().toString().trim();
    //geburtsdatum Type Date toString
    const geburtsdatum: string = geburtsdatumInput.val().toString().trim();
    const strasse: string = strasseInput.val().toString().trim();
    const plz: number = parseInt(plzInput.val().toString().trim());
    const wohnort: string = wohnortInput.val().toString().trim();
    const profilbild: string = "";

    const groups: any = userGroups;

    /* //Read which of the checkboxes are checked
        addUserForm.find('input[type="checkbox"]:checked').each((index, element) => {
            userGroups.push($(element).val());
            //console.log($(element).val());
        });
    */
    // Check if all required fields are filled in
    if (vorname && nachname) {
        $.ajax({
            url: '/user',
            type: 'POST',                           // POST route
            dataType: 'json',                       // expection json
            data: JSON.stringify({
                vorname,
                nachname,
                nutzername,
                passwort,
                email,
                geburtsdatum,
                strasse,
                plz,
                wohnort,
                profilbild,
                groups
            }),
            contentType: 'application/json',

            success: (response) => {
                // render Message
                renderMessage(response.message);        // render message
                // Update local user list (with groups)
                getUsers();
                //  getGroups();

                // Reset the values of all elements in the form
                addUserForm.trigger('reset');
                $("#btn_closeSignUp").trigger('click');
            },
            error: (jqXHRresponse) => {
                renderMessage(jqXHRresponse.responseJSON.message);
            },
        }).then(() => {
        });

    } else {
        // Not all required fields are filled in, print error message
        renderMessage('Not all fields are filled. Please check the form.');
    }
}

// Handler for editing a user in database
function editUser(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();

    // Define JQuery HTML objects
    const editModal: JQuery = $('#edit-user-modal');
    const editUserForm: JQuery = $('#edit-user-form');
    const firstNameInput: JQuery = $('#edit-first-name-input');
    const lastNameInput: JQuery = $('#edit-last-name-input');
    const idHiddenInput: JQuery = $('#edit-id-input');
    //const firstNameInput: JQuery = $('#edit-first-name-input');
    //const lastNameInput: JQuery = $('#edit-last-name-input');
    const editFirstName: JQuery = $('#editVorname');
    const editLastName: JQuery = $('#editNachname');
    const editUserName: JQuery = $('#editNutzername');
    const editPassword: JQuery = $('#editPasswort');
    const editEmail: JQuery = $('#editEmail');
    const editBirthdate: JQuery = $('#editGeburtsdatum');
    const editStreet: JQuery = $('#editStrasse');
    const editPlz: JQuery = $('#editPlz');
    const editPlace: JQuery = $('#editWohnort');


    // Read values from input fields
    const userId: number = Number(idHiddenInput.val().toString().trim());
    const vorname: string = editFirstName.val().toString().trim();
    const nachname: string = editLastName.val().toString().trim();
    const nutzername: string = editUserName.val().toString().trim();
    const passwort: string = editPassword.val().toString().trim();
    const email: string = editEmail.val().toString().trim();
    const geburtsdatum: string = editBirthdate.val().toString().trim();
    const strasse: string = editStreet.val().toString().trim();
    const plz: number = parseInt(editPlz.val().toString().trim());
    const wohnort: string = editPlace.val().toString().trim();


    // Check if all required fields are filled in
    if (vorname && nachname) {
        $.ajax({
            url: 'http://localhost:8080/user/' + userId,
            type: 'PUT',                            // PUT route
            dataType: 'json',                       // expection json
            data: JSON.stringify({
                vorname,
                nachname,
                nutzername,
                passwort,
                email,
                geburtsdatum,
                strasse,
                plz,
                wohnort
            }),
            contentType: 'application/json',

            success: (response) => {
                renderMessage(response.message);    // render Message
                // Update local user list
                getUsers();
                // Reset the values of all elements in the form
                editUserForm.trigger('reset');
            },
            error: (jqXHRresponse) => {
                renderMessage(jqXHRresponse.responseJSON.message);
            },
        }).then(() => {
        });

    } else {
        // Not all required fields are filled in, print error message
        renderMessage('Not all fields are filled. Please check the form');
    }
    editModal.modal('hide');
}

// Handler for editing a user in database
function editProfile(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();

    // Define JQuery HTML objects
    const editModal: JQuery = $('#edit-profile-modal');
    const editProfileForm: JQuery = $('#edit-profile-form');

    //const firstNameInput: JQuery = $('#edit-first-name-input');
    //const lastNameInput: JQuery = $('#edit-last-name-input');
    const editVorname: JQuery = $('#profilvorname');
    const editNachname: JQuery = $('#profilnachname');
    const editNutzername: JQuery = $('#profilnutzername');
    const editPasswort: JQuery = $('#profilpasswort');
    const editEmail: JQuery = $('#profilemail');
    const editGeburtsdatum: JQuery = $('#profilgeburtsdatum');
    const editStrasse: JQuery = $('#profilstrasse');
    const editPlz: JQuery = $('#profilplz');
    const editWohnort: JQuery = $('#profilwohnort');


    // Read values from input fields
    const vorname: string = editVorname.val().toString().trim();
    const nachname: string = editNachname.val().toString().trim();
    const nutzername: string = editNutzername.val().toString().trim();
    const passwort: string = editPasswort.val().toString().trim();
    const email: string = editEmail.val().toString().trim();
    const geburtsdatum: string = editGeburtsdatum.val().toString().trim();
    const strasse: string = editStrasse.val().toString().trim();
    const plz: number = parseInt(editPlz.val().toString().trim());
    const wohnort: string = editWohnort.val().toString().trim();


    $.ajax({
        url: '/userProfil',
        type: 'PUT',                            // PUT route
        dataType: 'json',                       // expection json
        data: JSON.stringify({
            vorname,
            nachname,
            nutzername,
            passwort,
            email,
            geburtsdatum,
            strasse,
            plz,
            wohnort
        }),
        contentType: 'application/json',

        success: (response) => {
            renderMessage(response.message);    // render Message
            // Update local user list
            // getCurrentUser()
            // getCurrentAdmin()
            // Reset the values of all elements in the form
            editProfileForm.trigger('reset');
            getCurrentAdmin()
            getCurrentUser()
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
    editModal.modal('hide');
}

//Handler for deleting a user in database
function deleteUser(event) {
    event.preventDefault();
    // Get user id from button attribute 'data-user-id'
    const userId: number = $(event.currentTarget).data('user-id');

    // Perform ajax request to log out user
    $.ajax({
        url: 'http://localhost:8080/user/' + userId,
        type: 'DELETE',                         // DELETE route
        dataType: 'json',                       // expection json
        success: (response) => {
            // render Message
            renderMessage(response.message);
            $("#delete-user-modal").modal("hide");
            // Get new user list from server
            getUsers();
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}

//Function for deleting the current User in database
function deleteCurrentUser(event) {
    event.preventDefault();
    // Get user id from button attribute 'data-user-id'

    // Perform ajax request to log out user
    $.ajax({
        url: 'http://localhost:8080/deleteProfile',
        type: 'DELETE',                         // DELETE route
        dataType: 'json',                       // expection json
        success: (response) => {
            // render Message
            renderMessage(response.message);
            // Get new user list from server
            logoutfunction()
            window.location.href = `../index.html`
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}

//Function for edit Menge(quantity) from tickets
function editMenge(event) {
    let mengeinput: JQuery = $('#ticketMenge');
    const menge: number = parseInt(mengeinput.val().toString().trim());
    const ticketId: number = $(event.currentTarget).data('ticket-id');


    $.ajax({
        url: 'http://localhost:8080/updateTicketMenge/' + ticketId,
        type: 'PUT',                         // Update route
        dataType: 'json',
        data: JSON.stringify({
            menge
        }),
        contentType: 'application/json',
        success: (response) => {
            // render Message
            renderMessage(response.message);
            // Get new user list from server
            getShoppingCart();
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
};

/* $('#sort_list').on('change', () => {
    let selection: JQuery = $('#sort_list')
    let changeSelector: string = String(selection.val());

    $.ajax({
        url: 'http://localhost:8080/login',
        type: 'GET',                            // GET request to check login and get user
        dataType: 'json',                       // expection json

        success: (response) => {

            $('#loggedInAsUser').append(" " + response.profile);

            switch (changeSelector) {
                case "Admin": {

                }
            }

            if (response.role === "Admin") {
                getCurrentAdmin();
            } else {
                getCurrentUser();
            }
            ;
            if (response.role !== "Admin") {
                getCurrentUser();
            } else {
                getCurrentAdmin();
            }

            renderMessage(response.message);    // render Message

        },
        error: (jqXHRresponse) => {
            $('#loginmodalbtn').show();
            $('#btnLogout').hide();
            $('#hintForLogin').show();
            $('#dropdown_Profil').hide()
            renderMessage(jqXHRresponse.responseJSON.message); // render Message
            $('#openUsermanagerbtn').hide();
        },
    })
})
*/

//Function for searching the userman in the searchbar
function searchUserman() {
    let searchbarinput: JQuery = $('#searchBoxUserman');
    let value: string = searchbarinput.val().toString().trim();

    let data = searchTable(value, userArray);
    renderUserList(data); //return the filtered list


}

//Function for  filtering the new userlist
function searchTable(value, data) {
    let filtered = []; //new array for the filtered users
    for (let i = 0; i < data.length; i++) {
        value = value.toLowerCase();	//to lowercase because it should ignore upper and lower letters
        let name = data[i].vorname.toLowerCase(); //declaring and init for the search
        let surname = data[i].nachname.toLowerCase();
        let username = data[i].nutzername.toLowerCase();

//check if the input value exists on firstname, lastname and username. If yes, it pushes this users to the filtered list
// into the data variable
        if (name.includes(value) || (surname.includes(value)) || (username.includes(value))) {
            filtered.push(data[i]);
        }
    }
    return filtered;
}

function watchStream(){

    $.ajax({
        url: '/checkWatchStream',
        type: 'GET',                            // GET route
        dataType: 'json',                       // expetion json
        success: (response) => {
            $('#watchStream').show();
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
            $('#watchStream').hide();
        },
    }).then(() => {
    });
}


/*****************************************************************************
 * Modal functions                                                          *
 *****************************************************************************/
//Function to open a modal window for editing the Profile
function openEditProfileModal() {
    // Get user id from button attribute 'data-user-id'
    // Perform ajax request to get the user with the clicked userId
    $.ajax({
        url: '/userProfil',
        type: 'GET',
        dataType: 'json',

        success: (response) => {
            //show the first user of array with 0
            renderEditProfileModal(response.userList);
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}

//Function to open a modal window for Login
function openloginmodal(event) {
    event.preventDefault();
    const loginmodal: JQuery = $("#loginmodal")
    loginmodal.modal("show");
}

//Function to open a modal window for edit a user
function openEditUserModal(event) {
    // Get user id from button attribute 'data-user-id'
    const userId: number = $(event.currentTarget).data('user-id');
    // Perform ajax request to get the user with the clicked userId
    $.ajax({
        url: 'http://localhost:8080/user/' + userId,
        type: 'GET',
        dataType: 'json',

        success: (response) => {
            //show the first user of array with 0
            renderEditUserModal(response.user[0]);
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}

/*****************************************************************************
 * Search functions                                                          *
 *****************************************************************************/

// Function search Festival in searchbar
function searchFestival(e) {
    e.preventDefault()

    const searchBar: JQuery = $('#searchbar');
    let query = searchBar.val()
    let url

    for (const searchFestival of searchFestivalarray) {
        if (searchFestival.name == query) {

            url = searchFestival.url
            window.location.href = `/${url}`
        }

    }
}

/*****************************************************************************
 * Render functions                                                          *
 *****************************************************************************/
// Function to render the Festival list //to fill searchFestivalarray
function renderFestivalList(festivalList: Festival[]) {
    // Define JQuery HTML objects
    const festivalTableBody: JQuery = $('#alle_festivals');
    const searchList: JQuery = $('#festival_search_list');
    // Delete the old table of users from the DOM
    festivalTableBody.empty();

    // For each user create a row and append it to the user table
    for (const festival of festivalList) {
        searchFestivalarray.push({
            name: festival.name,
            startdatum: festival.startdatum,
            url: festival.href,
            mindestpreis: festival.mindestpreis
        })
        // Create html table row element...
        const tableEntry: JQuery = $(`
             <div class="col-md-4">
                <img class="d-block w-100" src="${festival.bild}">
                <br>
                <p><b>${festival.name}</b></p>
                <p>${festival.datum}
                    <a id="btn_ZumFestival1" class="btn btn-outline-light float-right"
                       href="${festival.href}" role="button">zum Festival</a></p></p><br>
            </div>
        `);
        const searchEntry: JQuery = $(`
            <option value="${festival.name}"> 
        `)
        // ... and append it to the table's body
        festivalTableBody.append(tableEntry);
        searchList.append(searchEntry)
        //pushFestivals()
    }
}

// Function to render a message in all cases
function renderMessage(message: string) {
    // Define JQuery HTML Objects
    const messageWindow: JQuery = $('#messages');
    const messageModal: JQuery = $('#message_modal')
    // Create new alert
    const newAlert: JQuery = $(`
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            ${message}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    `);

    // Add message to DOM
    messageWindow.append(newAlert);
    messageModal.modal('show')
    // Auto-remove message after 5 seconds (5000ms)
    setTimeout(() => {
        newAlert.alert('close');
        messageModal.modal('hide')
    }, 3000);
}

//Function to get Tickets from database (Buchungspakete)
function renderTicketList(ticketList: Ticket[]) {
    // Define JQuery HTML objects
    const ticketTableBody: JQuery = $('#ticket-table-body');
    // Delete the old table of users from the DOM
    ticketTableBody.empty();

    // For each user create a row and append it to the user table
    for (const ticket of ticketList) {
        // Create html table row element...
        const tableEntry: JQuery = $(`
            <div class="buchungscontainer col-md">
                <h5>${ticket.beschreibung}</h5>
                <p id="tickettext">Du hast nicht das ganze Wochende Zeit?<br>
                    Kein Problem! Mit dem Tagesticket entscheidest du dich für einen Tag mit deinen Lieblingsbands.</p>
                <div class="ticketprice">
                    <p>${ticket.preis} € pro Ticket inkl. MwSt.
                    <div class="quantity">
                        <input type="number" class="count" name="qty" value="1">
                    </div>
                </div>
                </p>
                <div class="form-check form-check-inline">
                    <input type="checkbox" class="form-check-input" id="tagesticket-day1" value="option1">
                    <label class="form-check-label" for="tagesticket-day1">Tag 1</label>

                    <input type="checkbox" class="form-check-input" id="tagesticket-day2" value="option2">
                    <label class="form-check-label" for="tagesticket-day2">Tag 2</label>

                    <input type="checkbox" class="form-check-input" id="tagesticket-day3" value="option3">
                    <label class="form-check-label" for="tagesticket-day3">Tag 3</label>
                </div>
                <br><br>
                <button class="btn btn-red add-to-cart-btn" data-ticket-id="${ticket.ticketId}"><i class="fa fa-shopping-bag"></i></button>
            </div>
        `);
        // ... and append it to the table's body
        ticketTableBody.append(tableEntry);
    }
}

//Function to get all users from database
function renderUserList(userList: User[]) {
    // Define JQuery HTML objects
    const userTableBody: JQuery = $('#user-table-body');
    // Delete the old table of users from the DOM
    userTableBody.empty();

    // For each user create a row and append it to the user table
    for (const user of userList) {
        // Create html table row element...
        const tableEntry: JQuery = $(`
            <tr>
                <td>${user.benutzerId}</td>
                <td>${user.vorname}</td>
                <td>${user.nachname}</td>   
                <td>${user.nutzername}</td>  
                <td>${user.passwort}</td>  
                <td>${user.email}</td>  
                <td>${user.geburtsdatum}</td>  
                <td>${user.strasse}</td>  
                <td>${user.plz}</td>  
                <td>${user.wohnort}</td>  
                <td>
                    <button class="btn btn-outline-dark btn-sm edit-user-button mr-4" data-user-id="${user.benutzerId}" >
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-outline-dark btn-sm delete-user-button" data-user-id="${user.benutzerId}">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </td>
            </tr>
        `);
        // ... and append it to the table's body
        userTableBody.append(tableEntry);
    }
}

// Render User Profile
function renderUserProfil(userList: User[]) {
    // Define JQuery HTML objects
    const userTableBodyAf: JQuery = $('#user-profile-body-af');
    const userTableBodyUm: JQuery = $('#user-profile-body-um');
    const userTableBodyM: JQuery = $('#user-profile-body-m');

    // Delete the old table of users from the DOM
    userTableBodyAf.empty();
    userTableBodyUm.empty();
    userTableBodyM.empty();

    // For each user create a row and append it to the user table
    for (const user of userList) {
        // Create html table row element...
        const tableEntry: JQuery = $(`
            <div class="profile-header-top">
                <img class="profile-header-cover" id="bc_img" src="Pictures/Startbild1.png"/>
                <div class="profile-header-content">
                    <div class="profile-header-img">
                        <img src="${user.profilbild}"/>
                    </div>
                </div>
                <div class="profile-container">
                    <div class="profile-sidebar">
                        <div class="desktop-sticky-top">
                            <h4>${user.vorname} ${user.nachname}</h4>
                            <div class="font-weight-600 mb-3 text-muted mt-n2">@${user.nutzername}</div>
                            <!--Ort/email & Info
                            <p>
                                Principal UXUI Design
                            </p>
                            -->
                            <div class="mb-1"><i class="fa fa-map-marker-alt fa-fw text-muted"></i>${user.wohnort}</div>
                            <div class="mb-3"><i class="fa fa-envelope fa-fw text-muted"></i>${user.email}</div>
                        </div>
                    </div>
                </div>
                            <div id="profile_tabs">
                <ul class="profile-header-tab nav justify-content-center mt-5 ">
                    <li class="nav-item ml-5 mr-5" id="tab_alleFestivals">
                        <a href="profilFestivals.html" class="nav-link active" >
                            <div class="nav-field">Alle Festivals</div>
                        </a>
                    </li>
                    <li class="nav-item ml-5 mr-5" id="tab_mitteilungen">
                        <a href="profilMessages.html" class="nav-link text-muted">
                            <div class="nav-field">Mitteilungen</div>
                        </a>
                    </li>
                </ul>
            </div>
          </div>
                
        `);
        // ... and append it to the table's body
        userTableBodyAf.append(tableEntry);
    }
    for (const user of userList) {
        // Create html table row element...
        const tableEntry: JQuery = $(`
            <div class="profile-header-top">
                <img class="profile-header-cover" id="bc_img" src="Pictures/Startbild1.png"/>
                <div class="profile-header-content">
                    <div class="profile-header-img">
                        <img src="${user.profilbild}"/>
                    </div>
                </div>
                <div class="profile-container">
                    <div class="profile-sidebar">
                        <div class="desktop-sticky-top">
                            <h4>${user.vorname} ${user.nachname}</h4>
                            <div class="font-weight-600 mb-3 text-muted mt-n2">@${user.nutzername}</div>
                            <!--Ort/email & Info
                            <p>
                                Principal UXUI Design
                            </p>
                            -->
                            <div class="mb-1"><i class="fa fa-map-marker-alt fa-fw text-muted"></i>${user.wohnort}</div>
                            <div class="mb-3"><i class="fa fa-envelope fa-fw text-muted"></i>${user.email}</div>
                        </div>
                    </div>
                </div>
                            <div id="profile_tabs">
                <ul class="profile-header-tab nav justify-content-center mt-5 ">
                    <li class="nav-item ml-5 mr-5" id="tab_alleFestivals">
                        <a href="profilFestivals.html" class="nav-link text-muted" >
                            <div class="nav-field">Alle Festivals</div>
                        </a>
                    </li>
                    <li class="nav-item ml-5 mr-5" id="tab_mitteilungen">
                        <a href="profilMessages.html" class="nav-link active">
                            <div class="nav-field">Mitteilungen</div>
                        </a>
                    </li>
                </ul>
            </div>
          </div>
                
        `);
        // ... and append it to the table's body
        userTableBodyM.append(tableEntry);
    }
    for (const user of userList) {
        // Create html table row element...
        const tableEntry: JQuery = $(`
            <div class="profile-header-top">
                <img class="profile-header-cover" id="bc_img" src="Pictures/Startbild1.png"/>
                <div class="profile-header-content">
                    <div class="profile-header-img">
                        <img src="${user.profilbild}"/>
                    </div>
                </div>
                <div class="profile-container">
                    <div class="profile-sidebar">
                        <div class="desktop-sticky-top">
                            <h4>${user.vorname} ${user.nachname}</h4>
                            <div class="font-weight-600 mb-3 text-muted mt-n2">@${user.nutzername}</div>
                            <!--Ort/email & Info
                            <p>
                                Principal UXUI Design
                            </p>
                            -->
                            <div class="mb-1"><i class="fa fa-map-marker-alt fa-fw text-muted"></i>${user.wohnort}</div>
                            <div class="mb-3"><i class="fa fa-envelope fa-fw text-muted"></i>${user.email}</div>
                        </div>
                    </div>
                </div>
                            <div id="profile_tabs">
                <ul class="profile-header-tab nav justify-content-center mt-5 ">
                    <li class="nav-item ml-5 mr-5" id="tab_alleFestivals">
                        <a href="profilFestivals.html" class="nav-link text-muted" >
                            <div class="nav-field">Alle Festivals</div>
                        </a>
                    </li>
                    <li class="nav-item ml-5 mr-5" id="tab_mitteilungen">
                        <a href="profilMessages.html" class="nav-link text-muted">
                            <div class="nav-field">Mitteilungen</div>
                        </a>
                    </li>
                </ul>
            </div>
          </div>
                
        `);
        // ... and append it to the table's body
        userTableBodyUm.append(tableEntry);
    }
}

// Render User Profile
function renderAdminProfil(userList: User[]) {
    // Define JQuery HTML objects
    const userTableBodyAf: JQuery = $('#user-profile-body-af');
    const userTableBodyUm: JQuery = $('#user-profile-body-um');
    const userTableBodyM: JQuery = $('#user-profile-body-m');

    // Delete the old table of users from the DOM
    userTableBodyAf.empty();
    userTableBodyUm.empty();
    userTableBodyM.empty();

    // For each user create a row and append it to the user table
    for (const user of userList) {
        // Create html table row element...
        const tableEntry: JQuery = $(`
            <div class="profile-header-top">
                <img class="profile-header-cover" id="bc_img" src="Pictures/Startbild1.png"/>
                <div class="profile-header-content">
                    <div class="profile-header-img">
                        <img src="${user.profilbild}"/>
                    </div>
                </div>
                <div class="profile-container">
                    <div class="profile-sidebar">
                        <div class="desktop-sticky-top">
                            <h4>${user.vorname} ${user.nachname}</h4>
                            <div class="font-weight-600 mb-3 text-muted mt-n2">@${user.nutzername}</div>
                            <!--Ort/email & Info
                            <p>
                                Principal UXUI Design
                            </p>
                            -->
                            <div class="mb-1"><i class="fa fa-map-marker-alt fa-fw text-muted"></i>${user.wohnort}</div>
                            <div class="mb-3"><i class="fa fa-envelope fa-fw text-muted"></i>${user.email}</div>
                        </div>
                    </div>
                </div>
                            <div id="profile_tabs">
                <ul class="profile-header-tab nav justify-content-center mt-5 ">
                    <li class="nav-item ml-5 mr-5" id="tab_alleFestivals">
                        <a href="profilFestivals.html" class="nav-link active" >
                            <div class="nav-field">Alle Festivals</div>
                        </a>
                    </li>
                    <li class="nav-item ml-5 mr-5" id="tab_mitteilungen">
                        <a href="profilMessages.html" class="nav-link text-muted">
                            <div class="nav-field">Mitteilungen</div>
                        </a>
                    </li>
                    <li class="nav-item ml-5 mr-5" id="tab_userMan">
                        <a href="profileUsermanager.html" class="nav-link text-muted">
                            <div class="nav-field">User Manager</div>
                        </a>
                    </li>
                </ul>
            </div>
          </div>
                
        `);
        // ... and append it to the table's body
        userTableBodyAf.append(tableEntry);
    }
    for (const user of userList) {
        // Create html table row element...
        const tableEntry: JQuery = $(`
            <div class="profile-header-top">
                <img class="profile-header-cover" id="bc_img" src="Pictures/Startbild1.png"/>
                <div class="profile-header-content">
                    <div class="profile-header-img">
                        <img src="${user.profilbild}"/>
                    </div>
                </div>
                <div class="profile-container">
                    <div class="profile-sidebar">
                        <div class="desktop-sticky-top">
                            <h4>${user.vorname} ${user.nachname}</h4>
                            <div class="font-weight-600 mb-3 text-muted mt-n2">@${user.nutzername}</div>
                            <!--Ort/email & Info
                            <p>
                                Principal UXUI Design
                            </p>
                            -->
                            <div class="mb-1"><i class="fa fa-map-marker-alt fa-fw text-muted"></i>${user.wohnort}</div>
                            <div class="mb-3"><i class="fa fa-envelope fa-fw text-muted"></i>${user.email}</div>
                        </div>
                    </div>
                </div>
                            <div id="profile_tabs">
                <ul class="profile-header-tab nav justify-content-center mt-5 ">
                    <li class="nav-item ml-5 mr-5" id="tab_alleFestivals">
                        <a href="profilFestivals.html" class="nav-link text-muted" >
                            <div class="nav-field">Alle Festivals</div>
                        </a>
                    </li>
                    <li class="nav-item ml-5 mr-5" id="tab_mitteilungen">
                        <a href="profilMessages.html" class="nav-link active">
                            <div class="nav-field">Mitteilungen</div>
                        </a>
                    </li>
                    <li class="nav-item ml-5 mr-5" id="tab_userMan">
                        <a href="profileUsermanager.html" class="nav-link text-muted">
                            <div class="nav-field">User Manager</div>
                        </a>
                    </li>
                </ul>
            </div>
          </div>
                
        `);
        // ... and append it to the table's body
        userTableBodyM.append(tableEntry);
    }
    for (const user of userList) {
        // Create html table row element...
        const tableEntry: JQuery = $(`
            <div class="profile-header-top">
                <img class="profile-header-cover" id="bc_img" src="Pictures/Startbild1.png"/>
                <div class="profile-header-content">
                    <div class="profile-header-img">
                        <img src="${user.profilbild}"/>
                    </div>
                </div>
                <div class="profile-container">
                    <div class="profile-sidebar">
                        <div class="desktop-sticky-top">
                            <h4>${user.vorname} ${user.nachname}</h4>
                            <div class="font-weight-600 mb-3 text-muted mt-n2">@${user.nutzername}</div>
                            <!--Ort/email & Info
                            <p>
                                Principal UXUI Design
                            </p>
                            -->
                            <div class="mb-1"><i class="fa fa-map-marker-alt fa-fw text-muted"></i>${user.wohnort}</div>
                            <div class="mb-3"><i class="fa fa-envelope fa-fw text-muted"></i>${user.email}</div>
                        </div>
                    </div>
                </div>
                            <div id="profile_tabs">
                <ul class="profile-header-tab nav justify-content-center mt-5 ">
                    <li class="nav-item ml-5 mr-5" id="tab_alleFestivals">
                        <a href="profilFestivals.html" class="nav-link text-muted" >
                            <div class="nav-field">Alle Festivals</div>
                        </a>
                    </li>
                    <li class="nav-item ml-5 mr-5" id="tab_mitteilungen">
                        <a href="profilMessages.html" class="nav-link text-muted">
                            <div class="nav-field">Mitteilungen</div>
                        </a>
                    </li>
                    <li class="nav-item ml-5 mr-5" id="tab_userMan">
                        <a href="profileUsermanager.html" class="nav-link active">
                            <div class="nav-field">User Manager</div>
                        </a>
                    </li>
                </ul>
            </div>
          </div>
                
        `);
        // ... and append it to the table's body
        userTableBodyUm.append(tableEntry);
    }
}

// Function to render the Group list (Usermanager)
function renderGroupList(groupList: Group[]) {
    // Define JQuery HTML objects
    const userTableBody: JQuery = $('#group-table-body');
    // Delete the old table of users from the DOM
    userTableBody.empty();

    // For each user create a row and append it to the user table
    for (const group of groupList) {
        // Create html table row element...
        const tableEntry: JQuery = $(`
            <tr>
                <td class="text-info">${group.benutzergruppenId}</td>
                <td class="text-info">${group.beschreibung}</td>
                </td>
            </tr>
        `);
        // ... and append it to the table's body
        userTableBody.append(tableEntry);
    }
}

// Function to render the delete modal
function renderDeleteModal(userId: number) {
    const deleteModal: JQuery = $("#deleteUserModal");
    deleteModal.empty();


    const deleteButtonContent: JQuery = $(`
        <button class="btn btn-light btn-sm delete-button" data-user-id="${userId}">
             Nutzer löschen
                    </button>
      
    `)
    deleteModal.append(deleteButtonContent);



}

//Function to open the delete modal window
function openDeleteUserModal(event) {
    event.preventDefault();
    const userId: number = $(event.currentTarget).data('user-id');
    renderDeleteModal(userId);
    // Get user id from button attribute 'data-user-id'
    const deleteUserModal: JQuery = $("#delete-user-modal");

    // Perform ajax request to get the user with the clicked userId
    $.ajax({
        url: 'http://localhost:8080/user/' + userId,
        type: 'GET',
        dataType: 'json',

        success: (response) => {
            //show the Modal
            deleteUserModal.modal("show");
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}


// Function to render the edited user
function renderEditUserModal(user: User) {

    // Define JQuery HTML objects
    const editUserModal: JQuery = $('#edit-user-modal');
    const editIdInput: JQuery = $('#edit-id-input'); // Hidden field for saving the user's id
    const editFirstName: JQuery = $('#editVorname');
    const editLastName: JQuery = $('#editNachname');
    const editUserName: JQuery = $('#editNutzername');
    const editPassword: JQuery = $('#editPasswort');
    const editEmail: JQuery = $('#editEmail');
    const editBirthdate: JQuery = $('#editGeburtsdatum');
    const editStreet: JQuery = $('#editStrasse');
    const editPlz: JQuery = $('#editPlz');
    const editPlace: JQuery = $('#editWohnort');

    // Fill in edit fields in modal
    editIdInput.val(user.benutzerId);
    editFirstName.val(user.vorname);
    editLastName.val(user.nachname);
    editUserName.val(user.nutzername);
    editPassword.val(user.passwort);
    editEmail.val(user.email);
    editBirthdate.val(user.geburtsdatum);
    editStreet.val(user.strasse);
    editPlz.val(user.plz);
    editPlace.val(user.wohnort);

    renderModalGroups();
    renderModalGroupCkb(user.benutzerId);

    // Show modal
    editUserModal.modal('show');
}

// Function to render the edited Profile
function renderEditProfileModal(userList: User[]) {

    // Define JQuery HTML objects
    const editProfileModal: JQuery = $('#edit-profile-modal');
    const vorname: JQuery = $('#profilvorname');
    const nachname: JQuery = $('#profilnachname');
    const nutzername: JQuery = $('#profilnutzername');
    const passwort: JQuery = $('#profilpasswort');
    const email: JQuery = $('#profilemail');
    const geburtsdatum: JQuery = $('#profilgeburtsdatum');
    const strasse: JQuery = $('#profilstrasse');
    const plz: JQuery = $('#profilplz');
    const wohnort: JQuery = $('#profilwohnort');

    // Fill in edit fields in modal
    for (const user of userList) {
        vorname.val(user.vorname);
        nachname.val(user.nachname);
        nutzername.val(user.nutzername);
        passwort.val(user.passwort);
        email.val(user.email);
        geburtsdatum.val(user.geburtsdatum);
        strasse.val(user.strasse);
        plz.val(user.plz);
        wohnort.val(user.wohnort);
    }

    // Show modal
    editProfileModal.modal('show');
}

// Function to render the group of logged in user
function renderModalGroupCkb(userId) {
    const editUserForm: JQuery = $('#edit-user-form');

    $.ajax({
        url: "http://localhost:8080/user" + userId,
        type: "GET",
        dataType: "json",
        success: (response) => {
            let groups = response.user[0].benutzergruppenId
            editUserForm.find(`input[type="checkbox"][value="${groups}"]`).prop('checked', true);
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        }
    })
}

// Function to render all groups
function renderModalGroups() {
    const groupsEdit: JQuery = $('#groupsedit');

    $.ajax({
        url: "http://localhost:8080/groups",
        type: "GET",
        contentType: "application/JSON",
        dataType: "json",
        success: (response) => {
            groupsEdit.empty();
            let groups: any = response.groups;
            for (let group of groups) {
                let html: JQuery = $(`
                <div class="form-check">
                <input name="groups" type="checkbox" value="${group.benutzergruppenId}" class="form-check-input" id="ckb">
                <label class="form-check-label" for="ckb-${group.benutzergruppenId}">${group.beschreibung}</label>
                `);
                groupsEdit.append(html);
            }
        }
    })
}


// Function to render Festival List
function renderProfileFestivals(festivalList: Festival[]) {

    // Define JQuery HTML objects
    const festivalTableBodyAsc: JQuery = $('#profile_festivals_asc')
    const festivalTableBodyDsc: JQuery = $('#profile_festivals_desc');
    const today: Date = new Date()     //cannot get correct date-format

    // Delete the old table of users from the DOM
    festivalTableBodyAsc.empty();

    // For each user create a row and append it to the user table
    for (const festival of festivalList) {
        //get upcoming festivals
        if (festival.festivalId < 3) {
            // Create html table row element...
            const tableEntry: JQuery = $(`
            <div class="list-group-item d-flex align-items-center mb-5">
                   <img src="${festival.bild}" alt=""
                            width="150px" class="rounded-sm"/>
                   <div class="flex-fill pl-3 pr-3">
                     <div><a href="../${festival.href}" class="text-dark font-weight-600">${festival.name}</a>
                    </div>
                   <div class="text-muted fs-13px">${festival.datum}</div>
                </div>
                <a class="fas fa-ellipsis-h text-muted fa-2x"></a>
            </div>
            `);
            festivalTableBodyAsc.append(tableEntry);
        }
        // ... and append it to the table's body
    }

    for (const festival of festivalList.reverse() ) {
        //get past festivals
        if (festival.festivalId > 2) {
            // Create html table row element...
            const tableEntry: JQuery = $(`
            <div class="list-group-item d-flex align-items-center mb-5">
                   <img src="${festival.bild}" alt=""
                            width="150px" class="rounded-sm"/>
                   <div class="flex-fill pl-3 pr-3">
                     <div><a href="../${festival.href}" class="text-dark font-weight-600">${festival.name}</a>
                    </div>
                   <div class="text-muted fs-13px">${festival.datum}</div>
                </div>
                <a href="#" class="fas fa-ellipsis-h text-muted fa-2x"></a>
                    </div>
            `);
            // ... and append it to the table's body
            festivalTableBodyDsc.append(tableEntry);
        }
    }
}

// Function to hide and show the SignUpModal Window
function renderSignUpModal() {

    const loginModal: JQuery = $("#loginmodal");
    const signUpModal: JQuery = $("#signupmodal");

    loginModal.modal("hide");
    signUpModal.modal("show");
}

// Function to render the Shopping cart
function renderShoppingCart(shoppingList) {
    // Define JQuery HTML objects
    const shoppingTableBody: JQuery = $('#warenkorb_artikel');
    // Delete the old table of users from the DOM
    shoppingTableBody.empty();
    // For each user create a row and append it to the user table
    for (const list of shoppingList) {
        // Create html table row element...
        const tableEntry: JQuery = $(`
        <div id="Artikel2" style="height: 200px; width: 900px; margin: 10px 0 10px 0">
            <table>
                <tr>
                    <th><p><img id="Ticket2" style="width: 150px; height: 100px; margin-left: 10px;
            float: left; margin-top: 20px" src="Pictures/Ticket.jpg"></p>
                    </th>
                    <th>
                        <h2 style="margin-left: 20px; margin-top: 20px">Hurricane</h2>
                        <p id="Kuenstler2" style="margin-left: 20px; width: 350px">Rise Against/Seeed/Kings Of Leon</p>
                        <br>
                        <br>
                        <p id="Beschreibung2" style="margin-left: 20px">Erlebe die graue Stadt ohne Meer in Gießen!</p>
                        <br>
                        <br>
                        <p id="Dauer2" style="margin-left: 20px">Dauer: 1Tag</p>
                        <br>
                        <br>
                        <p id="Buchungspaket2" style="margin-left: 20px">- ${list.beschreibung}</p>
                    </th>
                    <th>
                    
                    <input id="ticketMenge" type="number" value=${list.menge}
                   
                  
                        <button id="mengeTicket" type="number" class="btn btn-dark change-ticket-btn" data-ticket-id="${list.ticketId}"
                                aria-haspopup="true" aria-expanded="false"
                                style="margin-bottom: 80px">
                        </button>
                        
                     
                    </th>
                    <th><h4 id="Preis2" style="margin-left: 80px; margin-bottom: 100px">${list.preis} €</h4>
                        <button id="löschen2" style="margin-left: 70px" class="delete-ticket-btn" data-ticket-id="${list.ticketId}"">
                            <i class="far fa-trash-alt"></i> löschen
                        </button>
                    </th>
                </tr>
            </table>
        </div>
        `);
        // ... and append it to the table's body
        shoppingTableBody.append(tableEntry);
    }
    calculateShoppingCart();
}

function calculateShoppingCart() {

}

// Function to hide/show title and timetable by clicking the FireBtn
function FireStage() {
    const FireStageTitle: JQuery = $('#FireStageTitle');
    const FireStageTimetable: JQuery = $('#FireStageTimetable');
    const WaterStageTitle: JQuery = $('#WaterStageTitle');
    const WaterStageTimetable: JQuery = $('#WaterStageTimetable');
    const EarthStageTitle: JQuery = $('#EarthStageTitle');
    const EarthStageTimetable: JQuery = $('#EarthStageTimetable');
    const AirStageTitle: JQuery = $('#AirStageTitle');
    const AirStageTimetable: JQuery = $('#AirStageTimetable');

    FireStageTitle.show();
    FireStageTimetable.show();
    WaterStageTitle.hide();
    WaterStageTimetable.hide();
    EarthStageTitle.hide();
    EarthStageTimetable.hide();
    AirStageTitle.hide();
    AirStageTimetable.hide();
}

// Function to hide/show title and timetable by clicking the WaterBtn
function WaterStage() {
    const FireStageTitle: JQuery = $('#FireStageTitle');
    const FireStageTimetable: JQuery = $('#FireStageTimetable');
    const WaterStageTitle: JQuery = $('#WaterStageTitle');
    const WaterStageTimetable: JQuery = $('#WaterStageTimetable');
    const EarthStageTitle: JQuery = $('#EarthStageTitle');
    const EarthStageTimetable: JQuery = $('#EarthStageTimetable');
    const AirStageTitle: JQuery = $('#AirStageTitle');
    const AirStageTimetable: JQuery = $('#AirStageTimetable');

    FireStageTitle.hide();
    FireStageTimetable.hide();
    WaterStageTitle.show();
    WaterStageTimetable.show();
    EarthStageTitle.hide();
    EarthStageTimetable.hide();
    AirStageTitle.hide();
    AirStageTimetable.hide();
}

// Function to hide/show title and timetable by clicking the EarthBtn
function EarthStage() {
    const FireStageTitle: JQuery = $('#FireStageTitle');
    const FireStageTimetable: JQuery = $('#FireStageTimetable');
    const WaterStageTitle: JQuery = $('#WaterStageTitle');
    const WaterStageTimetable: JQuery = $('#WaterStageTimetable');
    const EarthStageTitle: JQuery = $('#EarthStageTitle');
    const EarthStageTimetable: JQuery = $('#EarthStageTimetable');
    const AirStageTitle: JQuery = $('#AirStageTitle');
    const AirStageTimetable: JQuery = $('#AirStageTimetable');

    FireStageTitle.hide();
    FireStageTimetable.hide();
    WaterStageTitle.hide();
    WaterStageTimetable.hide();
    EarthStageTitle.show();
    EarthStageTimetable.show();
    AirStageTitle.hide();
    AirStageTimetable.hide();
}

// Function to hide/show title and timetable by clicking the AirBtn
function AirStage() {
    const FireStageTitle: JQuery = $('#FireStageTitle');
    const FireStageTimetable: JQuery = $('#FireStageTimetable');
    const WaterStageTitle: JQuery = $('#WaterStageTitle');
    const WaterStageTimetable: JQuery = $('#WaterStageTimetable');
    const EarthStageTitle: JQuery = $('#EarthStageTitle');
    const EarthStageTimetable: JQuery = $('#EarthStageTimetable');
    const AirStageTitle: JQuery = $('#AirStageTitle');
    const AirStageTimetable: JQuery = $('#AirStageTimetable');

    FireStageTitle.hide();
    FireStageTimetable.hide();
    WaterStageTitle.hide();
    WaterStageTimetable.hide();
    EarthStageTitle.hide();
    EarthStageTimetable.hide();
    AirStageTitle.show();
    AirStageTimetable.show();
}

// hide title and timetable of stage modal window for the first click
function hideTimetable() {
    const FireStageTitle: JQuery = $('#FireStageTitle');
    const FireStageTimetable: JQuery = $('#FireStageTimetable');
    const WaterStageTitle: JQuery = $('#WaterStageTitle');
    const WaterStageTimetable: JQuery = $('#WaterStageTimetable');
    const EarthStageTitle: JQuery = $('#EarthStageTitle');
    const EarthStageTimetable: JQuery = $('#EarthStageTimetable');
    const AirStageTitle: JQuery = $('#AirStageTitle');
    const AirStageTimetable: JQuery = $('#AirStageTimetable');

    FireStageTitle.hide();
    FireStageTimetable.hide();
    WaterStageTitle.hide();
    WaterStageTimetable.hide();
    EarthStageTitle.hide();
    EarthStageTimetable.hide();
    AirStageTitle.hide();
    AirStageTimetable.hide();
}

// callback functions
function sortUserList(event) {
    event.preventDefault()

    let selection: JQuery = $('#sort_id')
    let changeSelector: string = String(selection.val());

    userArray = userArray.sort((userA: User, userB: User) => {

        switch (changeSelector) {
            case "asc":
                if (userA.benutzerId < userB.benutzerId) {
                    return -1;
                }
                if (userA.benutzerId > userB.benutzerId) {
                    return 1;
                }
                return 0;

            case "desc":
                if (userA.benutzerId > userB.benutzerId) {
                    return -1;
                }
                if (userA.benutzerId < userB.benutzerId) {
                    return 1
                }
                return 0;
        }

    });
    renderUserList(userArray)
}

// callback functions
function sortFestivalList(event) {
    event.preventDefault()

    let selection: JQuery = $('#sort_festivals')
    let changeSelector: string = String(selection.val());


    sortFestivalarray = sortFestivalarray.sort((festivalA: Festival, festivalsB: Festival) => {

        switch (changeSelector) {
            case "Datum aufsteigend":
                if (festivalA.startdatum < festivalsB.startdatum) {
                    return -1;
                }
                if (festivalA.startdatum > festivalsB.startdatum) {
                    return 1;
                }
                return 0;

            case "Datum absteigend":
                if (festivalA.startdatum > festivalsB.startdatum) {
                    return -1;
                }
                if (festivalA.startdatum < festivalsB.startdatum) {
                    return 1
                }
                return 0;
            case "Preis aufsteigend":
                if (festivalA.mindestpreis < festivalsB.mindestpreis) {
                    return -1;
                }
                if (festivalA.mindestpreis > festivalsB.mindestpreis) {
                    return 1;
                }
                return 0;

            case "Preis absteigend":
                if (festivalA.mindestpreis > festivalsB.mindestpreis) {
                    return -1;
                }
                if (festivalA.mindestpreis < festivalsB.mindestpreis) {
                    return 1
                }
                return 0;
        }

    });
    renderFestivalList(sortFestivalarray)
}

/*****************************************************************************
 * Main Callback: Wait for DOM to be fully loaded                            *
 *****************************************************************************/
$(() => {

    // Define JQuery HTML objects, constanten deklarieren und zuweisen
    const loginmodalbtn: JQuery = $("#loginmodalbtn");
    const login: JQuery = $('#btnLogin');
    const logout: JQuery = $('#btnLogout');
    const signUpButton: JQuery = $("#btnSignup");
    const signUpLink: JQuery = $("#signuplink");
    const signUpLink1: JQuery = $("#signuplink1");
    const btnaddUser: JQuery = $('#btnaddUser');
    const addUserForm: JQuery = $('#add-user-form');
    const editUserForm: JQuery = $('#edit-user-form');
    const editProfileBtn: JQuery = $('#edit-profile-btn')
    const editProfileSubmit: JQuery = $('#edit-profile-submit-btn')
    const userTableBody: JQuery = $('#user-table-body');
    const ticketTableBody: JQuery = $('#ticket-table-body');
    const searchBarForm: JQuery = $('#searchForm');
    const Genrefilter: JQuery = $('#select-genre');
    const Resetfilter: JQuery = $('#filter-reset');
    const shoppingTableBody: JQuery = $('#warenkorb_artikel')
    const deleteUserButton: JQuery = $("#deleteUserButton");
    const deleteCurrentUserBtn: JQuery = $('#btnDeleteProfile')
    const ticketMenge: JQuery = $('#ticketMenge');
    const deleteUserModal: JQuery = $('#delete-user-modal');
    const sortUserListSel: JQuery = $('#sort_id')
    const sortFestivalListSel: JQuery = $('#sort_festivals');
    const searchbarUserman: JQuery = $('#searchBoxUserman');

    // check, if user is already logged in (e.g. after refresh)
    checkLogin();
    // a current festival list
    getFestivals();
    getGroups();
    getTickets();
    // hides title and timetable in stage modal window
    hideTimetable();

    // Register listeners
    sortFestivalListSel.on('change', sortFestivalList);
    sortUserListSel.on('change', sortUserList);
    loginmodalbtn.on('click', openloginmodal);
    login.on('click', loginfunction);
    logout.on('click', logoutfunction);
    signUpButton.on('click', addUser);
    signUpLink.on('click', renderSignUpModal);
    signUpLink1.on('click', renderSignUpModal);
    addUserForm.on('submit', addUser);       // Create-Usermanager
    btnaddUser.on('click', addUser);        // Create-Registrierung
    editUserForm.on('submit', editUser);   // Edit-Usermanager
    editProfileBtn.on('click', openEditProfileModal);
    editProfileSubmit.on('click', editProfile)
    userTableBody.on('click', '.edit-user-button', openEditUserModal); // Click listener for edit button

    userTableBody.on('click', '.delete-user-button', openDeleteUserModal,); // Click listener for delete button
    deleteUserModal.on('click', '.delete-button', deleteUser);
    deleteUserButton.on('click', deleteUser);
    deleteCurrentUserBtn.on('click', deleteCurrentUser)

    ticketTableBody.on('click', '.add-to-cart-btn', addIntoShoppingCart)
    shoppingTableBody.on('click', '.delete-ticket-btn', deleteTicket);
    searchBarForm.on('submit', searchFestival);
    Genrefilter.on('change', getGenre);
    Resetfilter.on('click', resetGenreFilter);
    shoppingTableBody.on('click', '.change-ticket-btn', editMenge)
    //ticketMenge.on('mouseup keyup', editMenge);
    searchbarUserman.on('change', searchUserman);
});