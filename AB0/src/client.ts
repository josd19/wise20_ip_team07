import {User, UserList} from './classes.js'

/****************************************************************************
 * global variables                                                          *
 *****************************************************************************/
let userList: UserList = new UserList();

/*****************************************************************************
 * Event Handlers (callbacks)                                                *
 *****************************************************************************/
function addUser(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();

    // Define JQuery HTML objects
    const addUserForm: JQuery = $('#add-user-form');
    const firstNameField: JQuery = $('#add-first-name-input');
    const lastNameField: JQuery = $('#add-last-name-input');
    const nameField: JQuery = $('#add-animal-name-input')
    const animalBirthField: JQuery = $('#add-animal-birthday-input')
    const animalGenderField: JQuery = $('#add-animal-gender-input')

    // Read values from input fields
    const firstName: string = firstNameField.val().toString().trim();
    const lastName: string = lastNameField.val().toString().trim();
    const animalName: string = nameField.val().toString().trim();
    //älteste Haustier funktioniert nicht mit date
    const animalBirth: number = Number(animalBirthField.val());
    const animalGender: string = animalGenderField.val().toString().trim();


    // Check if all required fields are filled in
    if (firstName && lastName) {
        // Create new user and add it to userList
        userList.addUser(new User(firstName, lastName, animalName, animalBirth, animalGender, new Date()));
        // Reset form by triggering "reset"-event
        addUserForm.trigger('reset');
        // Render message and user list
        renderMessage('User created');
        renderUserList(userList.getUsers());
    } else {
        renderMessage('Not all mandatory fields are filled in');
    }
}

function editUser(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();

    // Define JQuery HTML objects
    const editModal: JQuery = $('#edit-user-modal');
    const editUserForm: JQuery = $('#edit-user-form');
    const firstNameInput: JQuery = $('#edit-first-name-input');
    const lastNameInput: JQuery = $('#edit-last-name-input');
    const idHiddenInput: JQuery = $('#edit-id-input');

    // Read values from input fields
    const userId: number = Number(idHiddenInput.val().toString().trim());
    const firstName: string = firstNameInput.val().toString().trim();
    const lastName: string = lastNameInput.val().toString().trim();

    if (firstName && lastName) {
        if (userList.editUser(userId, firstName, lastName)) {
            // Clear form and close modal
            editUserForm.trigger('reset');
            editModal.modal('hide');
            // Render message and user list
            renderMessage(`Successfully updated user ${firstName} ${lastName}`);
            renderUserList(userList.getUsers());
        } else { // The user could not be found, send error response
            renderMessage('The user to be updated could not be found');
        }
    } else { // Either firstName or lastName is missing
        renderMessage('Not all mandatory fields are filled in');
    }
}

function deleteUser(event) {
    // Get user id from button attribute 'data-user-id'
    const userId: number = $(event.currentTarget).data('user-id');
    // delete user and render message
    if (userList.deleteUser(userId)) {
        renderMessage('Animal deleted');
    } else {
        renderMessage('The Animal to be deleted could not be found');
    }
    // render user list
    renderUserList(userList.getUsers());
}

function deleteAnimal(event) {
    const userId: number = $(event.currentTarget).data('user-id');

    if (userList.deleteAnimal(userId)) {
        renderMessage('Animal deleted');
    } else {
        renderMessage('The Animal to be deleted could not be found');
    }
    // render user list
    renderUserList(userList.getUsers());

}

function openEditUserModal(event) {
    // Get user id from button attribute 'data-user-id'
    const userId: number = $(event.currentTarget).data('user-id');

    // Search user in userList and show it in modal edit window
    let user: User = userList.getUser(userId);
    if (user !== null) { // user with userIds found in userList
        renderEditUserModal(user);
    } else { // user with userId not found in userList
        renderMessage('The selected user can not be found');
    }
}


/*****************************************************************************
 * Render functions                                                          *
 *****************************************************************************/
function renderMessage(message: string) {
    // Define JQuery HTML Objects
    const messageWindow: JQuery = $('#messages');

    // Create new alert
    const newAlert: JQuery = $(`
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            ${message}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    `);

    // Add message to DOM
    messageWindow.append(newAlert);

    // Auto-remove message after 5 seconds (5000ms)
    setTimeout(() => {
        newAlert.alert('close');
    }, 2000);
}

function renderUserList(userList: User[]) {
    // Define JQuery HTML objects
    const userTableBody: JQuery = $('#user-table-body');

    // Delete the old table of users from the DOM
    userTableBody.empty();
    // For each user create a row and append it to the user table
    for (const user of userList) {
        // Create html table row element...
        const tableEntry: JQuery = $(`
            <tr>
                <td>${user.id}</td>
                <td>${user.firstName}</td>
                <td>${user.lastName}</td>
                <td>${user.pet.animalName}</td>
                <td>${user.pet.year}</td>
                <td>${user.pet.gender}</td>
                <td>
                    <button class="btn btn-outline-dark btn-sm edit-user-button mr-4" data-user-id="${user.id}" >
                        <i class="fa fa-pen" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-outline-dark btn-sm delete-user-button" data-user-id="${user.id}">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </td>
                <td>
                    <button class="btn btn-outline-dark btn-sm delete-animal-button" data-user-id="${user.id}">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </td>
            </tr>
        `);
        // ... and append it to the table's body
        userTableBody.append(tableEntry);
    }
}

function renderEditUserModal(user: User) {
    // Define JQuery HTML objects
    const editUserModal: JQuery = $('#edit-user-modal');
    const editIdInput: JQuery = $('#edit-id-input'); // Hidden field for saving the user's id
    const editFirstNameInput: JQuery = $('#edit-first-name-input');
    const editLastNameInput: JQuery = $('#edit-last-name-input');

    // Fill in edit fields in modal
    editIdInput.val(user.id);
    editFirstNameInput.val(user.firstName);
    editLastNameInput.val(user.lastName);

    // Show modal
    editUserModal.modal('show');
}


/*****************************************************************************
 * Sort & Search functions                                                   *
 *****************************************************************************/

/**   searchFunction   **/
// new const for getUsers
let searchArray = (userList.getUsers());

//keyup event on searchBar
$('#search-bar').on('keyup', () => {
    // value from searchInput
    const searchValue: string = $('#search-bar').val().toString().trim();

    // data from searchUserList function
    const searchData = searchUserList(searchValue, userList.getUsers());

    renderUserList(searchData);
});

//main searchFunc
function searchUserList(searchValue, searchData) {

    // new array "searchData" for looping the searchArray (userList)
    const searchDataArray = [];

    // the loop will check each array entry if any of the searchValues is existing
    for (let i = 0; i < searchArray.length; i++) {
        searchValue = searchValue.toLowerCase();
        const firstName = searchData[i].firstName.toLowerCase();
        const lastName = searchData[i].lastName.toLowerCase();
        const animalName = searchData[i].pet.animalName.toLowerCase();
        const animalGender = searchData[i].pet.gender.toLowerCase()
        const animalBirthday = searchData[i].pet.year.toLowerCase()

        // if the searchValues matches the array entries the data will be pushed into the searchData array
        if (firstName.includes(searchValue) || (lastName.includes(searchValue)) || (animalName.includes(searchValue)) || (animalGender.includes(searchValue)) || (animalBirthday.includes(searchValue))) {
            searchDataArray.push(searchData[i]);
        }
    }
    return searchDataArray;
}


/**       sortFunction          **/
//filterUserList for sort function
let filterUserList = (userList.getUsers());

//onChange event/funcion for sorting userList
$('#sort__list').on('change', () => {
    //selector-element
    let selection: JQuery = $('#sort__list')
    //selector-val
    let changeSelector: string = String(selection.val());
    //sorting the userList
    filterUserList = filterUserList.sort((userA: User, userB: User) => {
        let firstUser: string = userA.firstName;
        let secondUser: string = userB.lastName;
        let firstUserLast: string = userA.lastName;
        let secondUserLast: string = userB.lastName;

        //switch statement for sorting types
        switch (changeSelector) {
            case "ID-aufsteigen":
                if (userA.id < userB.id) {
                    return -1;
                }
                if (userA.id > userB.id) {
                    return 1;
                }
                return 0;

            case "ID-absteigend":
                if (userA.id > userB.id) {
                    return -1;
                }
                if (userA.id < userB.id) {
                    return 1
                }
                return 0;

            case "Vorname-aufsteigend":
                if (firstUser < secondUser) {
                    return -1;
                }
                if (firstUser > secondUser) {
                    return 1;
                }
                return 0;

            case "Vorname-absteigend":
                if (firstUser > secondUser) {
                    return -1;
                }
                if (firstUser < secondUser) {
                    return 1;
                }
                return 0;

            case "Nachname-aufsteigend":
                if (firstUserLast < secondUserLast) {
                    return -1;
                }
                if (firstUserLast > secondUserLast) {
                    return 1;
                }
                return 0;

            case "Nachname-absteigend":
                if (firstUserLast > secondUserLast) {
                    return -1;
                }
                if (firstUserLast < secondUserLast) {
                    return 1;
                }
                return 0;
        }
    });
    //render the sorted userList
    renderUserList(filterUserList)
})

// sortBtn example (instead of select-dropdown)
$('#sort-first-up').on('click', () => {
    filterUserList = filterUserList.sort((userA: User, userB: User) => {
        let firstUser: string = userA.firstName;
        let secondUser: string = userB.lastName;

        if (firstUser < secondUser) {
            return -1;
        }
        if (firstUser > secondUser) {
            return 1;
        }
        return 0;
    })
    renderUserList(filterUserList)
})


/*****************************************************************************
 *                              stats functions                              *
 *****************************************************************************/

//for loop to count Pets
//

//const animalStatic: JQuery = $('#stats-modal');

$('#stats__btn').on('click', () => {
    const statsModal: JQuery = $('#stats-modal');
    const petArray = [];
    let ownerCounter: number = 0;
    let d = new Date();
    let date = d.getFullYear();
    let summe: number = 0;
    let animalAge: number = 0;
    let average: number;

    for (const user of userList.getUsers()) {
        if (user.pet.animalName) {
            ownerCounter++;
            petArray.push(date-(user.pet.year));
            console.log(petArray)
        }
    }

    for (const user of userList.getUsers()) {
        if (user.pet.year) {
            animalAge = date - user.pet.year;
            summe = summe + animalAge;
        }
    }
    average = summe / ownerCounter;

    $("#animalAverage").text(average.toString())
    $("#animalOwner").text(ownerCounter.toString())
    $("#animalOldest").text((Math.max(...petArray)).toString())

    // Show modal
    statsModal.modal('show');
})




/*****************************************************************************
 * Main Callback: Wait for DOM to be fully loaded                            *
 *****************************************************************************/
$(() => {
    // Define JQuery HTML objects
    const addUserForm: JQuery = $('#add-user-form');
    const editUserForm: JQuery = $('#edit-user-form');
    const userTableBody: JQuery = $('#user-table-body');
    const selection: JQuery = $('#sort__list')


// debugger; /* sets a breakpoint for browser-debuggung */

    // Register listeners
    $('#add-user-btn').on('click', addUser);
    editUserForm.on('click', editUser);
    userTableBody.on('click', '.edit-user-button', openEditUserModal);
    userTableBody.on('click', '.delete-user-button', deleteUser);
    userTableBody.on('click', '.delete-animal-button', deleteAnimal);
});

